import React, { Component } from "react";
import { StyleSheet, View, Image, ImageBackground, Text } from "react-native";
import MaterialHeader1 from "../components/MaterialHeader1";
import Contact1new from "../components/Contact1new";
import History from "../components/History";
import Contact2 from "../components/Contact2";
import Contact3 from "../components/Contact3";
import Contact4 from "../components/Contact4";
import Contact5 from "../components/Contact5";
import Contact6 from "../components/Contact6";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import MaterialCheckbox6 from "../components/MaterialCheckbox6";
import Contact7 from "../components/Contact7";
import Contact8 from "../components/Contact8";
import Contact9 from "../components/Contact9";
import FooterNav from "../components/FooterNav";

export default class Leads extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View
          style={[
            styles.stack,
            {
              marginTop: -7,
              marginLeft: -226,
              height: 857,
              width: 827
            }
          ]}
        >
          <ImageBackground
            source={require("../assets/images/background.png")}
            resizeMode="contain"
            style={styles.image}
          >
            <View
              style={[
                styles.stack,
                {
                  marginTop: 40,
                  marginLeft: 226,
                  height: 77,
                  width: 375
                }
              ]}
            >
              <MaterialHeader1 style={styles.materialHeader1} />
              <Image
                source={require("../assets/images/digital-catalog-logo.png")}
                resizeMode="contain"
                style={styles.image2}
              />
            </View>
            <View
              style={[
                styles.stack,
                {
                  marginTop: 25,
                  marginLeft: 226,
                  height: 91,
                  width: 375
                }
              ]}
            >
              <Contact1new style={styles.contact1New} />
              <History style={styles.history} />
            </View>
            <Contact2 style={styles.contact2} />
            <View
              style={[
                styles.stack,
                {
                  marginTop: 1,
                  marginLeft: 226,
                  height: 102,
                  width: 375
                }
              ]}
            >
              <Contact3 style={styles.contact3} />
              <Contact4 style={styles.contact4} />
            </View>
            <View
              style={[
                styles.stack,
                {
                  marginTop: 1,
                  marginLeft: 226,
                  height: 102,
                  width: 375
                }
              ]}
            >
              <Contact5 style={styles.contact5} />
              <Contact6 style={styles.contact6} />
              <Icon name="plus-circle" style={styles.icon5} />
              <MaterialCheckbox6 style={styles.materialCheckbox6} />
              <Text style={styles.text5}>Contact 6</Text>
            </View>

            <View
              style={[
                styles.stack,
                {
                  marginTop: 44,
                  marginLeft: 226,
                  height: 56,
                  width: 375
                }
              ]}
            >
              <FooterNav style={styles.materialBasicFooter1} />
              <View style={styles.rect10} />
            </View>
          </ImageBackground>
          <Image
            source={require("../assets/images/powerbycatalogos_logo.png")}
            resizeMode="contain"
            style={styles.image3}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  stack: {
    position: "relative"
  },
  image: {
    width: 827,
    height: 827,
    position: "absolute",
    left: 0,
    top: 0
  },
  materialHeader1: {
    top: 7,
    left: 0,
    width: 375,
    height: 60,
    position: "absolute"
  },
  image2: {
    top: 0,
    width: 126,
    height: 77,
    position: "absolute",
    left: 125
  },
  contact1New: {
    top: 40,
    left: 0,
    width: 375,
    height: 51,
    position: "absolute"
  },
  history: {
    top: 0,
    left: 0,
    width: 375,
    height: 40,
    position: "absolute"
  },
  contact2: {
    width: 375,
    height: 51,
    marginTop: 1,
    marginLeft: 226
  },
  contact3: {
    top: 0,
    left: 0,
    width: 375,
    height: 51,
    position: "absolute"
  },
  contact4: {
    top: 51,
    left: 0,
    width: 375,
    height: 51,
    position: "absolute"
  },
  contact5: {
    top: 0,
    left: 0,
    width: 375,
    height: 51,
    position: "absolute"
  },
  contact6: {
    top: 51,
    left: 0,
    width: 375,
    height: 51,
    position: "absolute"
  },
  icon5: {
    top: 62,
    left: 17,
    position: "absolute",
    color: "grey",
    fontSize: 30,
    height: 30,
    width: 30
  },
  materialCheckbox6: {
    top: 57,
    left: 315,
    width: 40,
    height: 40,
    position: "absolute"
  },
  text5: {
    top: 70,
    left: 60,
    color: "rgba(136,136,136,1)",
    position: "absolute",
    fontSize: 14,
    fontFamily: "roboto-regular"
  },
  contact7: {
    width: 375,
    height: 51,
    marginTop: 1,
    marginLeft: 226
  },
  contact8: {
    width: 375,
    height: 51,
    marginTop: 1,
    marginLeft: 226
  },
  contact9: {
    width: 375,
    height: 51,
    marginTop: 1,
    marginLeft: 226
  },
  materialBasicFooter1: {
    top: 0,
    left: 0,
    width: 375,
    height: 56,
    position: "absolute"
  },
  rect10: {
    top: 0,
    left: 102,
    width: 79,
    height: 56,
    backgroundColor: "rgba(255,255,255,1)",
    position: "absolute",
    opacity: 0.51
  },
  image3: {
    top: 682,
    left: 331,
    width: 175,
    height: 175,
    position: "absolute"
  }
});
