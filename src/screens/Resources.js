import React, { Component } from "react";
import { StyleSheet, View, Image, ImageBackground, Text } from "react-native";
import MaterialHeader1 from "../components/MaterialHeader1";
import ResourcesManagement from "../components/ResourcesManagement";
import Resource1 from "../components/Resource1";
import Resource2 from "../components/Resource2";
import Resource3 from "../components/Resource3";
import Resource4 from "../components/Resource4";
import Resource5 from "../components/Resource5";
import AddNewResourceButton from "../components/AddNewResourceButton";
import FooterNav from "../components/FooterNav";

export default class Resources extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View
          style={[
            styles.stack,
            {
              marginTop: -15,
              marginLeft: -226,
              height: 865,
              width: 827
            }
          ]}
        >
          <ImageBackground
            source={require("../assets/images/background.png")}
            resizeMode="contain"
            style={styles.image}
          >
            <View
              style={[
                styles.stack,
                {
                  marginTop: 48,
                  marginLeft: 226,
                  height: 77,
                  width: 375
                }
              ]}
            >
              <MaterialHeader1 style={styles.materialHeader1} />
              <Image
                source={require("../assets/images/digital-catalog-logo.png")}
                resizeMode="contain"
                style={styles.image2}
              />
            </View>
            <View
              style={[
                styles.stack,
                {
                  marginTop: 15,
                  marginLeft: 226,
                  height: 255,
                  width: 375
                }
              ]}
            >
              <ResourcesManagement style={styles.resourcesManagement} />
              <View style={styles.rect2}>
                <Text style={styles.text}>Company Resources</Text>
                <View
                  style={[
                    styles.stack,
                    {
                      marginTop: 6,
                      marginLeft: 20,
                      height: 152,
                      width: 286
                    }
                  ]}
                >
                  <Resource1 style={styles.resource1} />
                  <Resource2 style={styles.resource2} />
                  <Resource3 style={styles.resource3} />
                  <Resource4 style={styles.resource4} />
                  <Resource5 style={styles.resource5} />
                </View>
              </View>
            </View>
            <View style={styles.rect3}>
              <Text style={styles.text2}>My Resources</Text>
            </View>
            <AddNewResourceButton style={styles.materialButtonLight8} />
            <View
              style={[
                styles.stack,
                {
                  marginTop: 207,
                  marginLeft: 226,
                  height: 56,
                  width: 375
                }
              ]}
            >
              <FooterNav style={styles.footerNav} />
              <View style={styles.rect} />
            </View>
          </ImageBackground>
          <Image
            source={require("../assets/images/powerbycatalogos_logo.png")}
            resizeMode="contain"
            style={styles.image3}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  stack: {
    position: "relative"
  },
  image: {
    top: 0,
    left: 0,
    width: 827,
    height: 827,
    position: "absolute"
  },
  materialHeader1: {
    top: 7,
    left: 0,
    width: 375,
    height: 60,
    position: "absolute"
  },
  image2: {
    top: 0,
    width: 126,
    height: 77,
    position: "absolute",
    left: 125
  },
  resourcesManagement: {
    top: 0,
    left: 0,
    width: 375,
    height: 40,
    position: "absolute"
  },
  rect2: {
    top: 40,
    left: 0,
    width: 375,
    height: 215,
    backgroundColor: "rgba(255,255,255,1)",
    position: "absolute"
  },
  text: {
    color: "rgba(74,74,74,1)",
    marginTop: 29,
    marginLeft: 20
  },
  resource1: {
    top: 0,
    left: 0,
    width: 286,
    height: 40,
    position: "absolute"
  },
  resource2: {
    top: 27,
    left: 0,
    width: 189,
    height: 41,
    position: "absolute"
  },
  resource3: {
    top: 56,
    left: 1,
    width: 124,
    height: 41,
    position: "absolute"
  },
  resource4: {
    top: 83,
    left: 1,
    width: 197,
    height: 41,
    position: "absolute"
  },
  resource5: {
    top: 111,
    left: 1,
    width: 161,
    height: 41,
    position: "absolute"
  },
  rect3: {
    width: 375,
    height: 48,
    backgroundColor: "rgba(255,255,255,1)",
    marginTop: 1,
    marginLeft: 226
  },
  text2: {
    color: "rgba(74,74,74,1)",
    fontSize: 14,
    fontFamily: "arial-regular",
    marginTop: 17,
    marginLeft: 28
  },
  materialButtonLight8: {
    width: 189,
    height: 36,
    marginTop: 12,
    marginLeft: 254
  },
  footerNav: {
    top: 0,
    left: 0,
    width: 375,
    height: 56,
    position: "absolute"
  },
  rect: {
    top: 0,
    left: 194,
    width: 79,
    height: 56,
    backgroundColor: "rgba(255,255,255,1)",
    position: "absolute",
    opacity: 0.51
  },
  image3: {
    top: 690,
    left: 331,
    width: 175,
    height: 175,
    position: "absolute"
  }
});
