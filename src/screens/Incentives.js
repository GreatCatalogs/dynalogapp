import React, { Component } from "react";
import { StyleSheet, View, Image, ImageBackground } from "react-native";
import MaterialHeader1 from "../components/MaterialHeader1";
import Deals from "../components/Deals";
import IncentivesHeader from "../components/IncentivesHeader";
import Deals2 from "../components/Deals2";
import FooterNav from "../components/FooterNav";

export default class Incentives extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View
          style={[
            styles.stack,
            {
              marginTop: -7,
              marginLeft: -226,
              height: 857,
              width: 827
            }
          ]}
        >
          <ImageBackground
            source={require("../assets/images/background.png")}
            resizeMode="contain"
            style={styles.image}
          >
            <View
              style={[
                styles.stack,
                {
                  marginTop: 40,
                  marginLeft: 226,
                  height: 77,
                  width: 375
                }
              ]}
            >
              <MaterialHeader1 style={styles.materialHeader1} />
              <Image
                source={require("../assets/images/digital-catalog-logo.png")}
                resizeMode="contain"
                style={styles.image2}
              />
            </View>
            <View
              style={[
                styles.stack,
                {
                  marginTop: 13,
                  marginLeft: 215,
                  height: 206,
                  width: 386
                }
              ]}
            >
              <Deals style={styles.deals} />
              <IncentivesHeader style={styles.incentivesHeader} />
              <Deals2 style={styles.deals2} />
            </View>
            <FooterNav style={styles.footerNav} />
          </ImageBackground>
          <Image
            source={require("../assets/images/powerbycatalogos_logo.png")}
            resizeMode="contain"
            style={styles.image3}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  stack: {
    position: "relative"
  },
  image: {
    width: 827,
    height: 827,
    position: "absolute",
    left: 0,
    top: 0
  },
  materialHeader1: {
    top: 7,
    left: 0,
    width: 375,
    height: 60,
    position: "absolute"
  },
  image2: {
    top: 0,
    width: 126,
    height: 77,
    position: "absolute",
    left: 125
  },
  deals: {
    top: 25,
    left: 0,
    width: 386,
    height: 107,
    position: "absolute"
  },
  incentivesHeader: {
    top: 0,
    left: 11,
    width: 375,
    height: 42,
    position: "absolute"
  },
  deals2: {
    top: 111,
    left: 11,
    width: 375,
    height: 95,
    position: "absolute"
  },
  footerNav: {
    width: 375,
    height: 56,
    marginTop: 355,
    marginLeft: 226
  },
  image3: {
    top: 682,
    left: 331,
    width: 175,
    height: 175,
    position: "absolute"
  }
});
