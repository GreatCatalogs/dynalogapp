import React, { Component } from "react";
import { StyleSheet, View, Image, ImageBackground } from "react-native";

export default class Main extends Component {
  render() {
    return (
      <View style={styles.container}>
        <ImageBackground
          source={require("../assets/images/background.png")}
          resizeMode="contain"
          style={styles.image}
        >
          <ImageBackground
            source={require("../assets/images/digital-catalog-logo.png")}
            resizeMode="contain"
            style={styles.image2}
          >
            <Image
              source={require("../assets/images/powerbycatalogos_logo.png")}
              resizeMode="contain"
              style={styles.image3}
            />
          </ImageBackground>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  image: {
    width: 827,
    height: 827,
    marginTop: -9,
    marginLeft: -226
  },
  image2: {
    width: 496,
    height: 469,
    marginTop: 148,
    marginLeft: 166
  },
  image3: {
    width: 200,
    height: 200,
    marginTop: 249,
    marginLeft: 147
  }
});
