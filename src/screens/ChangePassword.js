import React, { Component } from "react";
import { StyleSheet, View, Image, ImageBackground } from "react-native";
import MaterialHeader1 from "../components/MaterialHeader1";
import PasswordChange from "../components/PasswordChange";
import NewPasswordChange from "../components/NewPasswordChange";
import RepeatPasswordChange from "../components/RepeatPasswordChange";
import MaterialButtonLight from "../components/MaterialButtonLight";
import FooterNav from "../components/FooterNav";

export default class ChangePassword extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View
          style={[
            styles.stack,
            {
              marginTop: -7,
              marginLeft: -226,
              height: 857,
              width: 827
            }
          ]}
        >
          <ImageBackground
            source={require("../assets/images/background.png")}
            resizeMode="contain"
            style={styles.image}
          >
            <View
              style={[
                styles.stack,
                {
                  marginTop: 40,
                  marginLeft: 226,
                  height: 77,
                  width: 375
                }
              ]}
            >
              <MaterialHeader1 style={styles.materialHeader1} />
              <Image
                source={require("../assets/images/digital-catalog-logo.png")}
                resizeMode="contain"
                style={styles.image2}
              />
            </View>
            <PasswordChange style={styles.passwordChangeHeader} />
            <NewPasswordChange style={styles.newPasswordChange} />
            <RepeatPasswordChange style={styles.repeatPasswordChange} />
            <MaterialButtonLight style={styles.materialButtonLight} />
            <FooterNav style={styles.footerNav} />
          </ImageBackground>
          <Image
            source={require("../assets/images/powerbycatalogos_logo.png")}
            resizeMode="contain"
            style={styles.image3}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  stack: {
    position: "relative"
  },
  image: {
    width: 827,
    height: 827,
    position: "absolute",
    left: 0,
    top: 0
  },
  materialHeader1: {
    top: 7,
    width: 375,
    height: 60,
    position: "absolute",
    left: 0
  },
  image2: {
    top: 0,
    width: 126,
    height: 77,
    position: "absolute",
    left: 125
  },
  passwordChangeHeader: {
    width: 375,
    height: 40,
    marginTop: 16,
    marginLeft: 226
  },
  newPasswordChange: {
    width: 311,
    height: 61,
    marginTop: 16,
    marginLeft: 258
  },
  repeatPasswordChange: {
    width: 311,
    height: 61,
    marginTop: 12,
    marginLeft: 258
  },
  materialButtonLight: {
    width: 100,
    height: 36,
    marginTop: 27,
    marginLeft: 363
  },
  footerNav: {
    width: 375,
    height: 56,
    marginTop: 305,
    marginLeft: 226
  },
  image3: {
    top: 682,
    left: 331,
    width: 175,
    height: 175,
    position: "absolute"
  }
});
