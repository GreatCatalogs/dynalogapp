import React, { Component } from "react";
import { StyleSheet, View, Image, ImageBackground } from "react-native";
import MaterialHeader1 from "../components/MaterialHeader1";
import Faqsheader from "../components/Faqsheader";
import Faq1 from "../components/Faq1";
import Faq2 from "../components/Faq2";
import Faq3 from "../components/Faq3";
import Faq4 from "../components/Faq4";
import Faq5 from "../components/Faq5";
import Faq6 from "../components/Faq6";
import Faq7 from "../components/Faq7";
import Faq8 from "../components/Faq8";
import Faq9 from "../components/Faq9";
import Faq10 from "../components/Faq10";
import Faq11 from "../components/Faq11";
import Faq12 from "../components/Faq12";
import Faq13 from "../components/Faq13";
import FooterNav from "../components/FooterNav";

export default class Faq extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View
          style={[
            styles.stack,
            {
              marginTop: -7,
              marginLeft: -226,
              height: 857,
              width: 827
            }
          ]}
        >
          <ImageBackground
            source={require("../assets/images/background.png")}
            resizeMode="contain"
            style={styles.image}
          >
            <View
              style={[
                styles.stack,
                {
                  marginTop: 40,
                  marginLeft: 226,
                  height: 77,
                  width: 375
                }
              ]}
            >
              <MaterialHeader1 style={styles.materialHeader1} />
              <Image
                source={require("../assets/images/digital-catalog-logo.png")}
                resizeMode="contain"
                style={styles.image2}
              />
            </View>
            <Faqsheader style={styles.faqsheader} />
            <Faq1 style={styles.faq1} />
            <Faq2 style={styles.faq2} />
            <Faq3 style={styles.faq3} />
            <Faq4 style={styles.faq4} />
            <Faq5 style={styles.faq5} />
            <Faq6 style={styles.faq6} />
            <Faq7 style={styles.faq7} />
            <Faq8 style={styles.faq8} />
            <Faq9 style={styles.faq9} />
            <Faq10 style={styles.faq10} />
            <Faq11 style={styles.faq11} />
            <Faq12 style={styles.faq12} />
            <Faq13 style={styles.faq13} />
            <FooterNav style={styles.footerNav} />
          </ImageBackground>
          <Image
            source={require("../assets/images/powerbycatalogos_logo.png")}
            resizeMode="contain"
            style={styles.image3}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  stack: {
    position: "relative"
  },
  image: {
    width: 827,
    height: 827,
    position: "absolute",
    left: 0,
    top: 0
  },
  materialHeader1: {
    top: 7,
    left: 0,
    width: 375,
    height: 60,
    position: "absolute"
  },
  image2: {
    top: 0,
    width: 126,
    height: 77,
    position: "absolute",
    left: 125
  },
  faqsheader: {
    width: 375,
    height: 40,
    marginTop: 13,
    marginLeft: 226
  },
  faq1: {
    width: 375,
    height: 36,
    marginTop: 2,
    marginLeft: 226
  },
  faq2: {
    width: 375,
    height: 36,
    marginTop: 1,
    marginLeft: 226
  },
  faq3: {
    width: 375,
    height: 36,
    marginTop: 1,
    marginLeft: 226
  },
  faq4: {
    width: 375,
    height: 36,
    marginTop: 1,
    marginLeft: 226
  },
  faq5: {
    width: 375,
    height: 36,
    marginTop: 1,
    marginLeft: 226
  },
  faq6: {
    width: 375,
    height: 36,
    marginTop: 1,
    marginLeft: 226
  },
  faq7: {
    width: 375,
    height: 36,
    marginTop: 1,
    marginLeft: 226
  },
  faq8: {
    width: 375,
    height: 36,
    marginTop: 1,
    marginLeft: 226
  },
  faq9: {
    width: 375,
    height: 36,
    marginTop: 1,
    marginLeft: 226
  },
  faq10: {
    width: 375,
    height: 36,
    marginTop: 1,
    marginLeft: 226
  },
  faq11: {
    width: 375,
    height: 36,
    marginTop: 1,
    marginLeft: 226
  },
  faq12: {
    width: 375,
    height: 36,
    marginTop: 1,
    marginLeft: 226
  },
  faq13: {
    width: 375,
    height: 36,
    marginTop: 1,
    marginLeft: 226
  },
  footerNav: {
    width: 375,
    height: 56,
    marginTop: 39,
    marginLeft: 226
  },
  image3: {
    top: 682,
    left: 331,
    width: 175,
    height: 175,
    position: "absolute"
  }
});
