import React, { Component } from "react";
import { StyleSheet, View, Image, ImageBackground, Text } from "react-native";
import MaterialHeader1 from "../components/MaterialHeader1";
import ContactUsHeader from "../components/ContactUsHeader";
import ContactUsMessage from "../components/ContactUsMessage";
import ContactUsSendMessage from "../components/ContactUsSendMessage";
import FooterNav from "../components/FooterNav";

export default class ContactUs extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View
          style={[
            styles.stack,
            {
              marginTop: -7,
              marginLeft: -226,
              height: 857,
              width: 827
            }
          ]}
        >
          <ImageBackground
            source={require("../assets/images/background.png")}
            resizeMode="contain"
            style={styles.image}
          >
            <View
              style={[
                styles.stack,
                {
                  marginTop: 40,
                  marginLeft: 226,
                  height: 77,
                  width: 375
                }
              ]}
            >
              <MaterialHeader1 style={styles.materialHeader1} />
              <Image
                source={require("../assets/images/digital-catalog-logo.png")}
                resizeMode="contain"
                style={styles.image2}
              />
            </View>
            <View
              style={[
                styles.stack,
                {
                  marginTop: 16,
                  marginLeft: 226,
                  height: 315,
                  width: 375
                }
              ]}
            >
              <ContactUsHeader style={styles.contactUsHeader} />
              <ContactUsMessage style={styles.materialUnderlineTextbox12} />
            </View>
            <ContactUsSendMessage style={styles.materialButtonLight} />
            <View
              style={[
                styles.stack,
                {
                  marginTop: 134,
                  marginLeft: 226,
                  height: 119,
                  width: 375
                }
              ]}
            >
              <FooterNav style={styles.footerNav} />
              <View style={styles.rect}>
                <Text style={styles.text}>
                  You can also:{"\n"}Email to support@catalogs.com{"\n"}Call to
                  954-908-7155.
                </Text>
              </View>
            </View>
          </ImageBackground>
          <Image
            source={require("../assets/images/powerbycatalogos_logo.png")}
            resizeMode="contain"
            style={styles.image3}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  stack: {
    position: "relative"
  },
  image: {
    width: 827,
    height: 827,
    position: "absolute",
    left: 0,
    top: 0
  },
  materialHeader1: {
    top: 7,
    width: 375,
    height: 60,
    position: "absolute",
    left: 0
  },
  image2: {
    top: 0,
    width: 126,
    height: 77,
    position: "absolute",
    left: 125
  },
  contactUsHeader: {
    top: 0,
    left: 0,
    width: 375,
    height: 40,
    position: "absolute"
  },
  materialUnderlineTextbox12: {
    top: 29,
    left: 0,
    width: 375,
    height: 286,
    position: "absolute"
  },
  materialButtonLight: {
    width: 151,
    height: 36,
    marginTop: 10,
    marginLeft: 343
  },
  footerNav: {
    top: 63,
    left: 0,
    width: 375,
    height: 56,
    position: "absolute"
  },
  rect: {
    top: 0,
    left: 0,
    width: 375,
    height: 63,
    backgroundColor: "rgba(255,255,255,1)",
    position: "absolute"
  },
  text: {
    color: "rgba(74,74,74,1)",
    fontSize: 14,
    fontFamily: "roboto-regular",
    lineHeight: 18,
    marginTop: 3,
    marginLeft: 20
  },
  image3: {
    top: 682,
    left: 331,
    width: 175,
    height: 175,
    position: "absolute"
  }
});
