import React, { Component } from "react";
import { StyleSheet, View, Image, ImageBackground, Text } from "react-native";
import MaterialHeader1 from "../components/MaterialHeader1";
import LeadName from "../components/LeadName";
import Icon from "react-native-vector-icons/Entypo";
import FooterNav from "../components/FooterNav";

export default class SendCatalog4 extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View
          style={[
            styles.stack,
            {
              marginTop: -7,
              marginLeft: -226,
              height: 857,
              width: 827
            }
          ]}
        >
          <ImageBackground
            source={require("../assets/images/background.png")}
            resizeMode="contain"
            style={styles.image}
          >
            <View
              style={[
                styles.stack,
                {
                  marginTop: 40,
                  marginLeft: 226,
                  height: 77,
                  width: 375
                }
              ]}
            >
              <MaterialHeader1 style={styles.materialHeader1} />
              <Image
                source={require("../assets/images/digital-catalog-logo.png")}
                resizeMode="contain"
                style={styles.image3}
              />
            </View>
            <View
              style={[
                styles.row,
                {
                  marginTop: 27,
                  marginLeft: 246,
                  marginRight: 410,
                  height: 36
                }
              ]}
            >
              <Text style={styles.text3}>Sent to:</Text>
              <LeadName style={styles.materialButtonPurple7} />
            </View>
            <View style={styles.rect2}>
              <View
                style={[
                  styles.row,
                  {
                    marginRight: 265,
                    marginLeft: 18,
                    marginTop: 5,
                    height: 27,
                    flex: 1
                  }
                ]}
              >
                <Icon name="folder-images" style={styles.icon} />
                <Text style={styles.text}>Thanks</Text>
              </View>
            </View>
            <View style={styles.rect}>
              <Text style={styles.text2}>
                Ask your customer to check their phone {"\n"}to be sure they got
                their email or text.
              </Text>
            </View>
            <View
              style={[
                styles.stack,
                {
                  marginTop: 404,
                  marginLeft: 226,
                  height: 56,
                  width: 375
                }
              ]}
            >
              <FooterNav style={styles.materialBasicFooter1} />
              <View style={styles.rect3} />
            </View>
          </ImageBackground>
          <Image
            source={require("../assets/images/powerbycatalogos_logo.png")}
            resizeMode="contain"
            style={styles.image2}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  stack: {
    position: "relative"
  },
  image: {
    width: 827,
    height: 827,
    position: "absolute",
    left: 0,
    top: 0
  },
  materialHeader1: {
    top: 7,
    left: 0,
    width: 375,
    height: 60,
    position: "absolute"
  },
  image3: {
    top: 0,
    width: 126,
    height: 77,
    position: "absolute",
    left: 125
  },
  row: {
    flexDirection: "row"
  },
  text3: {
    color: "#121212",
    marginTop: 11
  },
  materialButtonPurple7: {
    width: 110,
    height: 36,
    marginLeft: 11
  },
  rect2: {
    width: 375,
    height: 35,
    backgroundColor: "rgba(232,232,232,1)",
    flexDirection: "row",
    marginTop: 10,
    marginLeft: 226
  },
  icon: {
    color: "rgba(136,136,136,1)",
    fontSize: 27,
    height: 27,
    width: 20
  },
  text: {
    color: "rgba(74,74,74,1)",
    fontSize: 15,
    marginLeft: 18,
    marginTop: 6
  },
  rect: {
    width: 375,
    height: 61,
    backgroundColor: "rgba(255,255,255,1)",
    marginTop: 1,
    marginLeft: 226
  },
  text2: {
    color: "rgba(74,74,74,1)",
    fontSize: 15,
    marginTop: 14,
    marginLeft: 56
  },
  materialBasicFooter1: {
    top: 0,
    left: 0,
    width: 375,
    height: 56,
    position: "absolute"
  },
  rect3: {
    top: 0,
    left: 7,
    width: 79,
    height: 56,
    backgroundColor: "rgba(255,255,255,1)",
    position: "absolute",
    opacity: 0.51
  },
  image2: {
    top: 682,
    left: 331,
    width: 175,
    height: 175,
    position: "absolute"
  }
});
