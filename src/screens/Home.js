import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  ImageBackground,
  StatusBar
} from "react-native";
import MaterialHeader1 from "../components/MaterialHeader1";
import SendCatalog from "../components/SendCatalog";
import Catalog1 from "../components/Catalog1";
import Catalog2 from "../components/Catalog2";
import Catalog3 from "../components/Catalog3";
import Sendaresource from "../components/Sendaresource";
import Deals from "../components/Deals";
import Recentleads from "../components/Recentleads";
import Leadone from "../components/Leadone";
import Lead2 from "../components/Lead2";
import Lead3 from "../components/Lead3";
import FooterNav from "../components/FooterNav";

export default class Home extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View
          style={[
            styles.stack,
            {
              marginTop: -7,
              marginLeft: -226,
              height: 857,
              width: 827
            }
          ]}
        >
          <ImageBackground
            source={require("../assets/images/background.png")}
            resizeMode="contain"
            style={styles.image}
          >
            <View
              style={[
                styles.stack,
                {
                  marginTop: 40,
                  marginLeft: 226,
                  height: 77,
                  width: 375
                }
              ]}
            >
              <MaterialHeader1 style={styles.materialHeader1} />
              <Image
                source={require("../assets/images/digital-catalog-logo.png")}
                resizeMode="contain"
                style={styles.image2}
              />
            </View>
            <View
              style={[
                styles.stack,
                {
                  marginTop: 25,
                  marginLeft: 208,
                  height: 236,
                  width: 408
                }
              ]}
            >
              <View style={styles.rect}>
                <SendCatalog style={styles.sendCatalog} />
              </View>
              <Catalog1 style={styles.catalog1} />
              <Catalog2 style={styles.catalog2} />
              <Catalog3 style={styles.catalog3} />
            </View>
            <View
              style={[
                styles.stack,
                {
                  marginTop: 21,
                  marginLeft: 215,
                  height: 348,
                  width: 386
                }
              ]}
            >
              <View
                style={[
                  styles.stack,
                  {
                    top: 0,
                    left: 0,
                    height: 125,
                    width: 386,
                    position: "absolute"
                  }
                ]}
              >
                <View style={styles.rect2}>
                  <Sendaresource style={styles.sendaresource} />
                </View>
                <Deals style={styles.deals} />
              </View>
              <View
                style={[
                  styles.stack,
                  {
                    top: 102,
                    left: 11,
                    height: 246,
                    width: 375,
                    position: "absolute"
                  }
                ]}
              >
                <View style={styles.rect4}>
                  <Recentleads style={styles.recentleads} />
                  <Leadone style={styles.leadone} />
                  <Lead2 style={styles.lead2} />
                  <Lead3 style={styles.lead3} />
                </View>
                <FooterNav style={styles.materialBasicFooter1} />
                <View style={styles.rect5} />
              </View>
            </View>
          </ImageBackground>
          <Image
            source={require("../assets/images/powerbycatalogos_logo.png")}
            resizeMode="contain"
            style={styles.image3}
          />
        </View>
        <StatusBar animated={true} hidden={true} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  stack: {
    position: "relative"
  },
  image: {
    top: 0,
    left: 0,
    width: 827,
    height: 827,
    position: "absolute"
  },
  materialHeader1: {
    top: 7,
    left: 0,
    width: 375,
    height: 60,
    position: "absolute"
  },
  image2: {
    top: 0,
    width: 126,
    height: 77,
    position: "absolute",
    left: 125
  },
  rect: {
    top: 0,
    left: 18,
    width: 375,
    height: 35,
    backgroundColor: "rgba(232,232,232,1)",
    position: "absolute"
  },
  sendCatalog: {
    width: 183,
    height: 30,
    marginTop: 3,
    marginLeft: 21
  },
  catalog1: {
    top: 35,
    left: 0,
    width: 168,
    height: 201,
    position: "absolute"
  },
  catalog2: {
    top: 35,
    left: 121,
    width: 168,
    height: 201,
    position: "absolute"
  },
  catalog3: {
    top: 35,
    left: 240,
    width: 168,
    height: 201,
    position: "absolute"
  },
  rect2: {
    top: 0,
    left: 11,
    width: 375,
    height: 37,
    backgroundColor: "rgba(232,232,232,1)",
    position: "absolute"
  },
  sendaresource: {
    width: 330,
    height: 28,
    marginTop: 4,
    marginLeft: 23
  },
  deals: {
    top: 18,
    left: 0,
    width: 386,
    height: 107,
    position: "absolute"
  },
  rect4: {
    top: 0,
    left: 0,
    width: 375,
    height: 190,
    backgroundColor: "rgba(232,232,232,1)",
    position: "absolute"
  },
  recentleads: {
    width: 163,
    height: 34,
    marginTop: 13,
    marginLeft: 23
  },
  leadone: {
    width: 334,
    height: 32,
    marginTop: 7,
    marginLeft: 20
  },
  lead2: {
    width: 334,
    height: 32,
    marginTop: 6,
    marginLeft: 19
  },
  lead3: {
    width: 333,
    height: 32,
    marginTop: 8,
    marginLeft: 20
  },
  materialBasicFooter1: {
    top: 190,
    left: 0,
    width: 375,
    height: 56,
    position: "absolute"
  },
  rect5: {
    top: 190,
    left: 7,
    width: 79,
    height: 56,
    backgroundColor: "rgba(255,255,255,1)",
    position: "absolute",
    opacity: 0.51
  },
  image3: {
    top: 682,
    left: 331,
    width: 175,
    height: 175,
    position: "absolute"
  }
});
