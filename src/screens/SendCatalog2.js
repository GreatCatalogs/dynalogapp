import React, { Component } from "react";
import { StyleSheet, View, Image, ImageBackground, Text } from "react-native";
import MaterialHeader1 from "../components/MaterialHeader1";
import LeadName from "../components/LeadName";
import BrandInventorySampleButton from "../components/BrandInventorySampleButton";
import BrandTemplateSampleButton from "../components/BrandTemplateSampleButton";
import BrandCustomSampleButton from "../components/BrandCustomSampleButton";
import FooterNav from "../components/FooterNav";

export default class SendCatalog2 extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View
          style={[
            styles.stack,
            {
              marginTop: -7,
              marginLeft: -226,
              height: 857,
              width: 827
            }
          ]}
        >
          <ImageBackground
            source={require("../assets/images/background.png")}
            resizeMode="contain"
            style={styles.image}
          >
            <View
              style={[
                styles.stack,
                {
                  marginTop: 40,
                  marginLeft: 226,
                  height: 77,
                  width: 375
                }
              ]}
            >
              <MaterialHeader1 style={styles.materialHeader1} />
              <Image
                source={require("../assets/images/digital-catalog-logo.png")}
                resizeMode="contain"
                style={styles.image2}
              />
            </View>
            <View
              style={[
                styles.row,
                {
                  marginTop: 27,
                  marginLeft: 246,
                  marginRight: 410,
                  height: 36
                }
              ]}
            >
              <Text style={styles.text}>Send to:</Text>
              <LeadName style={styles.materialButtonPurple7} />
            </View>
            <BrandInventorySampleButton style={styles.materialButtonPurple9} />
            <BrandTemplateSampleButton style={styles.materialButtonPurple10} />
            <BrandCustomSampleButton style={styles.materialButtonPurple11} />
            <View
              style={[
                styles.stack,
                {
                  marginTop: 365,
                  marginLeft: 226,
                  height: 56,
                  width: 375
                }
              ]}
            >
              <FooterNav style={styles.materialBasicFooter1} />
              <View style={styles.rect} />
            </View>
          </ImageBackground>
          <Image
            source={require("../assets/images/powerbycatalogos_logo.png")}
            resizeMode="contain"
            style={styles.image3}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  stack: {
    position: "relative"
  },
  image: {
    top: 0,
    left: 0,
    width: 827,
    height: 827,
    position: "absolute"
  },
  materialHeader1: {
    top: 7,
    left: 0,
    width: 375,
    height: 60,
    position: "absolute"
  },
  image2: {
    top: 0,
    width: 126,
    height: 77,
    position: "absolute",
    left: 125
  },
  row: {
    flexDirection: "row"
  },
  text: {
    color: "#121212",
    marginTop: 11
  },
  materialButtonPurple7: {
    width: 110,
    height: 36,
    marginLeft: 7
  },
  materialButtonPurple9: {
    width: 329,
    height: 36,
    marginTop: 21,
    marginLeft: 246
  },
  materialButtonPurple10: {
    width: 329,
    height: 36,
    marginTop: 9,
    marginLeft: 246
  },
  materialButtonPurple11: {
    width: 329,
    height: 36,
    marginTop: 8,
    marginLeft: 246
  },
  materialBasicFooter1: {
    top: 0,
    left: 0,
    width: 375,
    height: 56,
    position: "absolute"
  },
  rect: {
    top: 0,
    left: 7,
    width: 79,
    height: 56,
    backgroundColor: "rgba(255,255,255,1)",
    position: "absolute",
    opacity: 0.51
  },
  image3: {
    top: 682,
    left: 331,
    width: 175,
    height: 175,
    position: "absolute"
  }
});
