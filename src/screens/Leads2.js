import React, { Component } from "react";
import { StyleSheet, View, Image, ImageBackground } from "react-native";
import MaterialHeader1 from "../components/MaterialHeader1";
import Lead1EditChat from "../components/Lead1EditChat";
import MyLeads from "../components/MyLeads";
import MaterialCheckbox10 from "../components/MaterialCheckbox10";
import MaterialCommunityIconsIcon from "react-native-vector-icons/MaterialCommunityIcons";
import Lead2EditChat from "../components/Lead2EditChat";
import Lead3EditChat from "../components/Lead3EditChat";
import Lead4EditChat from "../components/Lead4EditChat";
import Lead5EditChat from "../components/Lead5EditChat";
import Lead6EditChat from "../components/Lead6EditChat";
import Lead7EditChat from "../components/Lead7EditChat";
import Lead8EditChat from "../components/Lead8EditChat";
import Lead9EditChat from "../components/Lead9EditChat";
import FooterNav from "../components/FooterNav";

export default class Leads2 extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View
          style={[
            styles.stack,
            {
              marginTop: -7,
              marginLeft: -226,
              height: 857,
              width: 827
            }
          ]}
        >
          <ImageBackground
            source={require("../assets/images/background.png")}
            resizeMode="contain"
            style={styles.image}
          >
            <View
              style={[
                styles.stack,
                {
                  marginTop: 40,
                  marginLeft: 226,
                  height: 77,
                  width: 375
                }
              ]}
            >
              <MaterialHeader1 style={styles.materialHeader1} />
              <Image
                source={require("../assets/images/digital-catalog-logo.png")}
                resizeMode="contain"
                style={styles.image3}
              />
            </View>
            <View
              style={[
                styles.stack,
                {
                  marginTop: 25,
                  marginLeft: 226,
                  height: 90,
                  width: 375
                }
              ]}
            >
              <View style={styles.rect3}>
                <Lead1EditChat style={styles.lead1EditChat} />
              </View>
              <MyLeads style={styles.myLeads} />
            </View>
            <View
              style={[
                styles.stack,
                {
                  marginTop: 1,
                  marginLeft: 226,
                  height: 50,
                  width: 375
                }
              ]}
            >
              <MaterialCheckbox10 style={styles.materialCheckbox102} />
              <MaterialCommunityIconsIcon name="account" style={styles.icon2} />
              <View style={styles.rect4}>
                <Lead2EditChat style={styles.lead2EditChat} />
              </View>
            </View>
            <View
              style={[
                styles.stack,
                {
                  marginTop: 1,
                  marginLeft: 226,
                  height: 50,
                  width: 375
                }
              ]}
            >
              <MaterialCheckbox10 style={styles.materialCheckbox104} />
              <MaterialCommunityIconsIcon name="account" style={styles.icon4} />
              <View style={styles.rect5}>
                <Lead3EditChat style={styles.lead3EditChat} />
              </View>
            </View>
            <View style={styles.rect6}>
              <Lead4EditChat style={styles.lead4EditChat} />
            </View>
            <View style={styles.rect7}>
              <Lead5EditChat style={styles.lead5EditChat} />
            </View>
            <View style={styles.rect8}>
              <Lead6EditChat style={styles.lead6EditChat} />
            </View>
            <View style={styles.rect9}>
              <Lead7EditChat style={styles.lead7EditChat} />
            </View>
            <View style={styles.rect10}>
              <Lead8EditChat style={styles.lead8EditChat} />
            </View>
            <View style={styles.rect11}>
              <Lead9EditChat style={styles.lead9EditChat} />
            </View>
            <View
              style={[
                styles.stack,
                {
                  marginTop: 51,
                  marginLeft: 226,
                  height: 56,
                  width: 375
                }
              ]}
            >
              <View style={styles.rect}>
                <View style={styles.rect2} />
              </View>
              <FooterNav style={styles.footerNav} />
            </View>
          </ImageBackground>
          <Image
            source={require("../assets/images/powerbycatalogos_logo.png")}
            resizeMode="contain"
            style={styles.image2}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  stack: {
    position: "relative"
  },
  image: {
    top: 0,
    left: 0,
    width: 827,
    height: 827,
    position: "absolute"
  },
  materialHeader1: {
    top: 7,
    left: 0,
    width: 375,
    height: 60,
    position: "absolute"
  },
  image3: {
    top: 0,
    width: 126,
    height: 77,
    position: "absolute",
    left: 125
  },
  rect3: {
    top: 40,
    left: 0,
    width: 375,
    height: 50,
    backgroundColor: "rgba(255,255,255,1)",
    position: "absolute"
  },
  lead1EditChat: {
    width: 362,
    height: 40,
    marginTop: 5,
    marginLeft: 4
  },
  myLeads: {
    top: 0,
    left: 0,
    width: 375,
    height: 40,
    position: "absolute"
  },
  materialCheckbox102: {
    top: 5,
    left: 4,
    width: 40,
    height: 40,
    position: "absolute"
  },
  icon2: {
    top: 10,
    left: 36,
    position: "absolute",
    color: "grey",
    fontSize: 30,
    height: 30,
    width: 30
  },
  rect4: {
    top: 0,
    left: 0,
    width: 375,
    height: 50,
    backgroundColor: "rgba(255,255,255,1)",
    position: "absolute"
  },
  lead2EditChat: {
    width: 362,
    height: 40,
    marginTop: 4,
    marginLeft: 4
  },
  materialCheckbox104: {
    top: 4,
    left: 4,
    width: 40,
    height: 40,
    position: "absolute"
  },
  icon4: {
    top: 9,
    left: 36,
    position: "absolute",
    color: "grey",
    fontSize: 30,
    height: 30,
    width: 30
  },
  rect5: {
    top: 0,
    left: 0,
    width: 375,
    height: 50,
    backgroundColor: "rgba(255,255,255,1)",
    position: "absolute"
  },
  lead3EditChat: {
    width: 362,
    height: 40,
    marginTop: 4,
    marginLeft: 4
  },
  rect6: {
    width: 375,
    height: 50,
    backgroundColor: "rgba(255,255,255,1)",
    marginTop: 1,
    marginLeft: 226
  },
  lead4EditChat: {
    width: 362,
    height: 40,
    marginTop: 5,
    marginLeft: 4
  },
  rect7: {
    width: 375,
    height: 50,
    backgroundColor: "rgba(255,255,255,1)",
    marginTop: 1,
    marginLeft: 226
  },
  lead5EditChat: {
    width: 362,
    height: 40,
    marginTop: 5,
    marginLeft: 4
  },
  rect8: {
    width: 375,
    height: 50,
    backgroundColor: "rgba(255,255,255,1)",
    marginTop: 1,
    marginLeft: 226
  },
  lead6EditChat: {
    width: 362,
    height: 40,
    marginTop: 5,
    marginLeft: 4
  },
  rect9: {
    width: 375,
    height: 50,
    backgroundColor: "rgba(255,255,255,1)",
    marginTop: 1,
    marginLeft: 226
  },
  lead7EditChat: {
    width: 362,
    height: 40,
    marginTop: 5,
    marginLeft: 4
  },
  rect10: {
    width: 375,
    height: 50,
    backgroundColor: "rgba(255,255,255,1)",
    marginTop: 1,
    marginLeft: 226
  },
  lead8EditChat: {
    width: 362,
    height: 40,
    marginTop: 5,
    marginLeft: 4
  },
  rect11: {
    width: 375,
    height: 50,
    backgroundColor: "rgba(255,255,255,1)",
    marginTop: 1,
    marginLeft: 226
  },
  lead9EditChat: {
    width: 362,
    height: 40,
    marginTop: 5,
    marginLeft: 4
  },
  rect: {
    top: 0,
    left: 102,
    width: 79,
    height: 56,
    backgroundColor: "rgba(255,255,255,1)",
    position: "absolute",
    opacity: 0.51
  },
  rect2: {
    width: 79,
    height: 56,
    backgroundColor: "rgba(255,255,255,1)",
    opacity: 0.51
  },
  footerNav: {
    top: 0,
    left: 0,
    width: 375,
    height: 56,
    position: "absolute"
  },
  image2: {
    top: 682,
    left: 331,
    width: 175,
    height: 175,
    position: "absolute"
  }
});
