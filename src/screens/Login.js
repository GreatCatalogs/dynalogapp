import React, { Component } from "react";
import { StyleSheet, View, Image, ImageBackground } from "react-native";
import Email from "../components/Email";
import Password from "../components/Password";
import MaterialButtonLight2 from "../components/MaterialButtonLight2";
import Facebooklogin from "../components/Facebooklogin";
import Gmaillogin from "../components/Gmaillogin";

export default class Login extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View
          style={[
            styles.stack,
            {
              marginTop: -9,
              marginLeft: -226,
              height: 870,
              width: 940
            }
          ]}
        >
          <ImageBackground
            source={require("../assets/images/background.png")}
            resizeMode="contain"
            style={styles.image3}
          >
            <ImageBackground
              source={require("../assets/images/digital-catalog-logo.png")}
              resizeMode="contain"
              style={styles.image}
            >
              <Email style={styles.materialFixedLabelTextbox1} />
              <Password style={styles.materialFixedLabelTextbox2} />
            </ImageBackground>
            <MaterialButtonLight2 style={styles.materialButtonLight2} />
            <View
              style={[
                styles.stack,
                {
                  marginTop: 15,
                  marginLeft: 300,
                  height: 230,
                  width: 225
                }
              ]}
            >
              <Image
                source={require("../assets/images/powerbycatalogos_logo.png")}
                resizeMode="contain"
                style={styles.image2}
              />
              <Facebooklogin style={styles.materialButtonLight6} />
              <Gmaillogin style={styles.materialButtonLight6} />
            </View>
          </ImageBackground>
          <Image
            source={require("../assets/images/powerbycatalogos_logo.png")}
            resizeMode="contain"
            style={styles.image4}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  stack: {
    position: "relative"
  },
  image3: {
    top: 0,
    left: 0,
    width: 827,
    height: 827,
    position: "absolute"
  },
  image: {
    width: 496,
    height: 469,
    marginTop: 25,
    marginLeft: 166
  },
  materialFixedLabelTextbox1: {
    width: 282,
    height: 36,
    marginTop: 378,
    marginLeft: 106
  },
  materialFixedLabelTextbox2: {
    width: 282,
    height: 35,
    marginTop: 13,
    marginLeft: 106
  },
  materialButtonLight2: {
    width: 100,
    height: 36,
    marginTop: 14,
    marginLeft: 363
  },
  image2: {
    top: 30,
    left: 13,
    width: 200,
    height: 200,
    position: "absolute"
  },
  materialButtonLight6: {
    top: 49,
    left: 1,
    width: 224,
    height: 36,
    position: "absolute"
  },
  image4: {
    top: 670,
    left: 740,
    width: 200,
    height: 200,
    position: "absolute"
  }
});
