import React, { Component } from "react";
import { StyleSheet, View, Image, ImageBackground } from "react-native";
import MaterialHeader1 from "../components/MaterialHeader1";
import CarCatalog1 from "../components/CarCatalog1";
import ViewMyCarCatalog from "../components/ViewMyCarCatalog";
import CarCatalog3 from "../components/CarCatalog3";
import FooterNav from "../components/FooterNav";

export default class ViewCatalogs extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View
          style={[
            styles.stack,
            {
              marginTop: -7,
              marginLeft: -226,
              height: 857,
              width: 1026
            }
          ]}
        >
          <ImageBackground
            source={require("../assets/images/background.png")}
            resizeMode="contain"
            style={styles.image}
          >
            <View
              style={[
                styles.stack,
                {
                  marginTop: 40,
                  marginLeft: 226,
                  height: 77,
                  width: 375
                }
              ]}
            >
              <MaterialHeader1 style={styles.materialHeader1} />
              <Image
                source={require("../assets/images/digital-catalog-logo.png")}
                resizeMode="contain"
                style={styles.image2}
              />
            </View>
            <View
              style={[
                styles.stack,
                {
                  marginTop: 13,
                  marginLeft: 226,
                  height: 210,
                  width: 375
                }
              ]}
            >
              <CarCatalog1 style={styles.carCatalog1} />
              <ViewMyCarCatalog style={styles.viewMyCarCatalog} />
            </View>
            <View
              style={[
                styles.stack,
                {
                  marginTop: 3,
                  marginLeft: 226,
                  height: 342,
                  width: 375
                }
              ]}
            >
              <CarCatalog3 style={styles.carCatalog3} />
              <Image
                source={require("../assets/images/catalog-sample31.png")}
                resizeMode="contain"
                style={styles.image6}
              />
            </View>
            <View
              style={[
                styles.stack,
                {
                  marginTop: 6,
                  marginLeft: 226,
                  height: 56,
                  width: 375
                }
              ]}
            >
              <FooterNav style={styles.footerNav} />
              <View style={styles.rect} />
            </View>
          </ImageBackground>
          <Image
            source={require("../assets/images/powerbycatalogos_logo.png")}
            resizeMode="contain"
            style={styles.image3}
          />
          <View style={styles.rect6} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  stack: {
    position: "relative"
  },
  image: {
    width: 827,
    height: 827,
    position: "absolute",
    left: 0,
    top: 0
  },
  materialHeader1: {
    top: 7,
    left: 0,
    width: 375,
    height: 60,
    position: "absolute"
  },
  image2: {
    top: 0,
    width: 126,
    height: 77,
    position: "absolute",
    left: 125
  },
  carCatalog1: {
    top: 40,
    left: 0,
    width: 375,
    height: 170,
    position: "absolute"
  },
  viewMyCarCatalog: {
    top: 0,
    left: 0,
    width: 375,
    height: 40,
    position: "absolute"
  },
  carCatalog3: {
    top: 0,
    left: 0,
    width: 375,
    height: 342,
    position: "absolute"
  },
  image6: {
    top: 172,
    left: 0,
    width: 170,
    height: 170,
    position: "absolute"
  },
  footerNav: {
    top: 0,
    left: 0,
    width: 375,
    height: 56,
    position: "absolute"
  },
  rect: {
    top: 0,
    left: 287,
    width: 79,
    height: 56,
    backgroundColor: "rgba(255,255,255,1)",
    position: "absolute",
    opacity: 0.51
  },
  image3: {
    top: 682,
    left: 331,
    width: 175,
    height: 175,
    position: "absolute"
  },
  rect6: {
    top: 47,
    left: 651,
    width: 375,
    height: 60,
    backgroundColor: "rgba(230, 230, 230,1)",
    position: "absolute"
  }
});
