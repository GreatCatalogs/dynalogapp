import React, { Component } from "react";
import { StyleSheet, View, Image, ImageBackground, Text } from "react-native";
import MaterialHeader1 from "../components/MaterialHeader1";
import LeadName from "../components/LeadName";
import SelectResource from "../components/SelectResource";
import Resource1 from "../components/Resource1";
import Resource2 from "../components/Resource2";
import Resource3 from "../components/Resource3";
import Resource4 from "../components/Resource4";
import Resource5 from "../components/Resource5";
import SendButton from "../components/SendButton";
import FooterNav from "../components/FooterNav";

export default class SendCatalog3 extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View
          style={[
            styles.stack,
            {
              marginTop: -7,
              marginLeft: -226,
              height: 857,
              width: 827
            }
          ]}
        >
          <ImageBackground
            source={require("../assets/images/background.png")}
            resizeMode="contain"
            style={styles.image4}
          >
            <View
              style={[
                styles.stack,
                {
                  marginTop: 40,
                  marginLeft: 226,
                  height: 77,
                  width: 375
                }
              ]}
            >
              <MaterialHeader1 style={styles.materialHeader1} />
              <Image
                source={require("../assets/images/digital-catalog-logo.png")}
                resizeMode="contain"
                style={styles.image2}
              />
            </View>
            <View
              style={[
                styles.row,
                {
                  marginTop: 27,
                  marginLeft: 246,
                  marginRight: 410,
                  height: 36
                }
              ]}
            >
              <Text style={styles.text}>Send to:</Text>
              <LeadName style={styles.materialButtonPurple7} />
            </View>
            <SelectResource style={styles.selectResource} />
            <View style={styles.rect2}>
              <Text style={styles.text3}>Company Resources</Text>
              <View
                style={[
                  styles.stack,
                  {
                    marginTop: 6,
                    marginLeft: 18,
                    height: 152,
                    width: 286
                  }
                ]}
              >
                <Resource1 style={styles.resource1} />
                <Resource2 style={styles.resource2} />
                <Resource3 style={styles.resource3} />
                <Resource4 style={styles.resource4} />
                <Resource5 style={styles.resource5} />
              </View>
              <SendButton style={styles.materialButtonPurple12} />
            </View>
            <View
              style={[
                styles.stack,
                {
                  marginTop: 225,
                  marginLeft: 226,
                  height: 56,
                  width: 375
                }
              ]}
            >
              <FooterNav style={styles.materialBasicFooter1} />
              <View style={styles.rect3} />
            </View>
          </ImageBackground>
          <Image
            source={require("../assets/images/powerbycatalogos_logo.png")}
            resizeMode="contain"
            style={styles.image3}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  stack: {
    position: "relative"
  },
  image4: {
    top: 0,
    left: 0,
    width: 827,
    height: 827,
    position: "absolute"
  },
  materialHeader1: {
    top: 7,
    left: 0,
    width: 375,
    height: 60,
    position: "absolute"
  },
  image2: {
    top: 0,
    width: 126,
    height: 77,
    position: "absolute",
    left: 125
  },
  row: {
    flexDirection: "row"
  },
  text: {
    color: "#121212",
    marginTop: 11
  },
  materialButtonPurple7: {
    width: 110,
    height: 36,
    marginLeft: 7
  },
  selectResource: {
    width: 375,
    height: 35,
    marginTop: 10,
    marginLeft: 226
  },
  rect2: {
    width: 375,
    height: 240,
    backgroundColor: "rgba(255,255,255,1)",
    marginTop: 1,
    marginLeft: 226
  },
  text3: {
    color: "rgba(74,74,74,1)",
    marginTop: 11,
    marginLeft: 18
  },
  resource1: {
    top: 0,
    left: 0,
    width: 286,
    height: 40,
    position: "absolute"
  },
  resource2: {
    top: 27,
    left: 0,
    width: 189,
    height: 41,
    position: "absolute"
  },
  resource3: {
    top: 56,
    left: 1,
    width: 124,
    height: 41,
    position: "absolute"
  },
  resource4: {
    top: 83,
    left: 1,
    width: 197,
    height: 41,
    position: "absolute"
  },
  resource5: {
    top: 111,
    left: 1,
    width: 161,
    height: 41,
    position: "absolute"
  },
  materialButtonPurple12: {
    width: 100,
    height: 36,
    marginTop: 8,
    marginLeft: 30
  },
  materialBasicFooter1: {
    top: 0,
    left: 0,
    width: 375,
    height: 56,
    position: "absolute"
  },
  rect3: {
    top: 0,
    left: 7,
    width: 79,
    height: 56,
    backgroundColor: "rgba(255,255,255,1)",
    position: "absolute",
    opacity: 0.51
  },
  image3: {
    top: 682,
    left: 331,
    width: 175,
    height: 175,
    position: "absolute"
  }
});
