import React, { Component } from "react";
import { StyleSheet, View, Image, ImageBackground, Text } from "react-native";
import MaterialCommunityIconsIcon from "react-native-vector-icons/MaterialCommunityIcons";
import Logout from "../components/Logout";
import Profile from "../components/Profile";
import Leads from "../components/Leads";
import Resources from "../components/Resources";
import Incentives from "../components/Incentives";
import Faq from "../components/Faq";
import Support from "../components/Support";
import MassEmail from "../components/MassEmail";
import ChangePassword from "../components/ChangePassword";
import FooterNav from "../components/FooterNav";
import Users from "../components/Users";

export default class Menu extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View
          style={[
            styles.stack,
            {
              marginTop: -7,
              marginLeft: -226,
              height: 857,
              width: 827
            }
          ]}
        >
          <ImageBackground
            source={require("../assets/images/background.png")}
            resizeMode="contain"
            style={styles.image}
          >
            <View
              style={[
                styles.stack,
                {
                  marginTop: 49,
                  marginLeft: 226,
                  height: 172,
                  width: 375
                }
              ]}
            >
              <View style={styles.rect}>
                <MaterialCommunityIconsIcon name="close" style={styles.icon} />
              </View>
              <View style={styles.rect2}>
                <View
                  style={[
                    styles.row,
                    {
                      marginRight: 48,
                      marginLeft: 17,
                      marginTop: 17,
                      height: 81,
                      flex: 1
                    }
                  ]}
                >
                  <MaterialCommunityIconsIcon
                    name="account-box-outline"
                    style={styles.icon2}
                  />
                  <Logout style={styles.logout} />
                </View>
              </View>
            </View>
            <Profile style={styles.profile} />
            <Leads style={styles.leads} />
            <Resources style={styles.resources} />
            <Incentives style={styles.incentives} />
            <Faq style={styles.faq} />
            <Support style={styles.support} />
            <MassEmail style={styles.massEmail} />
            <ChangePassword style={styles.changePassword} />
            <View
              style={[
                styles.stack,
                {
                  marginTop: 2,
                  marginLeft: 226,
                  height: 107,
                  width: 375
                }
              ]}
            >
              <FooterNav style={styles.footerNav} />
              <Users style={styles.users} />
              <MaterialCommunityIconsIcon
                name="account-group"
                style={styles.icon12}
              />
              <Text style={styles.text10}>Users</Text>
            </View>
          </ImageBackground>
          <Image
            source={require("../assets/images/powerbycatalogos_logo.png")}
            resizeMode="contain"
            style={styles.image2}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  stack: {
    position: "relative"
  },
  image: {
    width: 827,
    height: 827,
    position: "absolute",
    left: 0,
    top: 0
  },
  rect: {
    top: 0,
    left: 0,
    width: 375,
    height: 59,
    backgroundColor: "rgba(38,98,128,1)",
    position: "absolute"
  },
  icon: {
    color: "rgba(255,255,255,1)",
    fontSize: 40,
    height: 40,
    width: 29,
    marginTop: 10,
    marginLeft: 17
  },
  rect2: {
    top: 59,
    left: 0,
    width: 375,
    height: 113,
    backgroundColor: "rgba(62,145,188,1)",
    position: "absolute",
    flexDirection: "row"
  },
  row: {
    flexDirection: "row"
  },
  icon2: {
    color: "rgba(255,255,255,1)",
    fontSize: 81,
    height: 81,
    width: 59
  },
  logout: {
    width: 82,
    height: 30,
    marginLeft: 169,
    marginTop: 29
  },
  profile: {
    width: 375,
    height: 51,
    marginTop: 1,
    marginLeft: 226
  },
  leads: {
    width: 375,
    height: 51,
    marginTop: 1,
    marginLeft: 226
  },
  resources: {
    width: 375,
    height: 51,
    marginTop: 1,
    marginLeft: 226
  },
  incentives: {
    width: 375,
    height: 51,
    marginTop: 1,
    marginLeft: 226
  },
  faq: {
    width: 375,
    height: 51,
    marginTop: 2,
    marginLeft: 226
  },
  support: {
    width: 375,
    height: 51,
    marginTop: 1,
    marginLeft: 226
  },
  massEmail: {
    width: 375,
    height: 51,
    marginTop: 1,
    marginLeft: 226
  },
  changePassword: {
    width: 375,
    height: 51,
    marginTop: 1,
    marginLeft: 226
  },
  footerNav: {
    top: 51,
    left: 0,
    width: 375,
    height: 56,
    position: "absolute"
  },
  users: {
    top: 0,
    left: 0,
    width: 375,
    height: 51,
    position: "absolute"
  },
  icon12: {
    top: 10,
    left: 316,
    position: "absolute",
    color: "rgba(255,255,255,1)",
    fontSize: 30,
    height: 30,
    width: 22
  },
  text10: {
    top: 16,
    left: 31,
    color: "rgba(255,255,255,1)",
    position: "absolute",
    fontSize: 18,
    fontFamily: "roboto-regular"
  },
  image2: {
    top: 682,
    left: 331,
    width: 175,
    height: 175,
    position: "absolute"
  }
});
