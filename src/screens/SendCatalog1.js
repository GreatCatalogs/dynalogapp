import React, { Component } from "react";
import { StyleSheet, View, Image, ImageBackground } from "react-native";
import MaterialHeader1 from "../components/MaterialHeader1";
import NewLeadButton from "../components/NewLeadButton";
import MyContactButton from "../components/MyContactButton";
import NewleadFirstName from "../components/NewleadFirstName";
import NewleadLastName from "../components/NewleadLastName";
import NewleadEmail from "../components/NewleadEmail";
import NewleadCellPhone from "../components/NewleadCellPhone";
import NewleadYear from "../components/NewleadYear";
import NewleadMake from "../components/NewleadMake";
import NewleadModel from "../components/NewleadModel";
import NewleadType from "../components/NewleadType";
import NewleadMyNotes from "../components/NewleadMyNotes";
import NextButton from "../components/NextButton";
import FooterNav from "../components/FooterNav";

export default class SendCatalog1 extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View
          style={[
            styles.stack,
            {
              marginTop: -7,
              marginLeft: -226,
              height: 857,
              width: 827
            }
          ]}
        >
          <ImageBackground
            source={require("../assets/images/background.png")}
            resizeMode="contain"
            style={styles.image}
          >
            <View
              style={[
                styles.stack,
                {
                  marginTop: 40,
                  marginLeft: 226,
                  height: 77,
                  width: 375
                }
              ]}
            >
              <MaterialHeader1 style={styles.materialHeader1} />
              <Image
                source={require("../assets/images/digital-catalog-logo.png")}
                resizeMode="contain"
                style={styles.image2}
              />
            </View>
            <View
              style={[
                styles.row,
                {
                  marginTop: 25,
                  marginLeft: 246,
                  marginRight: 352,
                  height: 37
                }
              ]}
            >
              <NewLeadButton style={styles.materialButtonPurple5} />
              <MyContactButton style={styles.materialButtonLight} />
            </View>
            <NewleadFirstName style={styles.newleadFirstName} />
            <NewleadLastName style={styles.newleadLastName} />
            <NewleadEmail style={styles.newleadEmail} />
            <NewleadCellPhone style={styles.newleadCellPhone} />
            <NewleadYear style={styles.newleadYear} />
            <NewleadMake style={styles.newleadMake} />
            <NewleadModel style={styles.newleadModel} />
            <NewleadType style={styles.newleadType} />
            <NewleadMyNotes style={styles.newleadMyNotes} />
            <NextButton style={styles.materialButtonPurple6} />
            <View
              style={[
                styles.stack,
                {
                  marginTop: 23,
                  marginLeft: 226,
                  height: 56,
                  width: 375
                }
              ]}
            >
              <FooterNav style={styles.materialBasicFooter1} />
              <View style={styles.rect} />
            </View>
          </ImageBackground>
          <Image
            source={require("../assets/images/powerbycatalogos_logo.png")}
            resizeMode="contain"
            style={styles.image3}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  stack: {
    position: "relative"
  },
  image: {
    width: 827,
    height: 827,
    position: "absolute",
    left: 0,
    top: 0
  },
  materialHeader1: {
    top: 7,
    left: 0,
    width: 375,
    height: 60,
    position: "absolute"
  },
  image2: {
    top: 0,
    width: 126,
    height: 77,
    position: "absolute",
    left: 125
  },
  row: {
    flexDirection: "row"
  },
  materialButtonPurple5: {
    width: 100,
    height: 36,
    marginTop: 1
  },
  materialButtonLight: {
    width: 119,
    height: 36,
    marginLeft: 10
  },
  newleadFirstName: {
    width: 315,
    height: 33,
    marginTop: 30,
    marginLeft: 253
  },
  newleadLastName: {
    width: 312,
    height: 33,
    marginTop: 10,
    marginLeft: 256
  },
  newleadEmail: {
    width: 282,
    height: 33,
    marginTop: 11,
    marginLeft: 286
  },
  newleadCellPhone: {
    width: 312,
    height: 33,
    marginTop: 11,
    marginLeft: 256
  },
  newleadYear: {
    width: 274,
    height: 33,
    marginTop: 10,
    marginLeft: 294
  },
  newleadMake: {
    width: 278,
    height: 33,
    marginTop: 10,
    marginLeft: 291
  },
  newleadModel: {
    width: 283,
    height: 33,
    marginTop: 11,
    marginLeft: 285
  },
  newleadType: {
    width: 277,
    height: 33,
    marginTop: 11,
    marginLeft: 291
  },
  newleadMyNotes: {
    width: 332,
    height: 64,
    marginTop: 10,
    marginLeft: 236
  },
  materialButtonPurple6: {
    width: 100,
    height: 36,
    marginTop: 11,
    marginLeft: 331
  },
  materialBasicFooter1: {
    top: 0,
    left: 0,
    width: 375,
    height: 56,
    position: "absolute"
  },
  rect: {
    top: 0,
    left: 7,
    width: 79,
    height: 56,
    backgroundColor: "rgba(255,255,255,1)",
    position: "absolute",
    opacity: 0.51
  },
  image3: {
    top: 682,
    left: 331,
    width: 175,
    height: 175,
    position: "absolute"
  }
});
