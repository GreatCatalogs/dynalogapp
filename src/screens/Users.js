import React, { Component } from "react";
import { StyleSheet, View, Image, ImageBackground } from "react-native";
import MaterialHeader1 from "../components/MaterialHeader1";
import Usersheader from "../components/Usersheader";
import User1info from "../components/User1info";
import User2info from "../components/User2info";
import User3info from "../components/User3info";
import User4info from "../components/User4info";
import User5info from "../components/User5info";
import AddUserButton from "../components/AddUserButton";
import FooterNav from "../components/FooterNav";

export default class Users extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View
          style={[
            styles.stack,
            {
              marginTop: -7,
              marginLeft: -226,
              height: 857,
              width: 827
            }
          ]}
        >
          <ImageBackground
            source={require("../assets/images/background.png")}
            resizeMode="contain"
            style={styles.image}
          >
            <View
              style={[
                styles.stack,
                {
                  marginTop: 40,
                  marginLeft: 226,
                  height: 77,
                  width: 375
                }
              ]}
            >
              <MaterialHeader1 style={styles.materialHeader1} />
              <Image
                source={require("../assets/images/digital-catalog-logo.png")}
                resizeMode="contain"
                style={styles.image2}
              />
            </View>
            <Usersheader style={styles.usersheader} />
            <User1info style={styles.user1Info} />
            <User2info style={styles.user2Info} />
            <User3info style={styles.user3Info} />
            <User4info style={styles.user4Info} />
            <User5info style={styles.user5Info} />
            <AddUserButton style={styles.materialButtonLight} />
            <FooterNav style={styles.footerNav} />
          </ImageBackground>
          <Image
            source={require("../assets/images/powerbycatalogos_logo.png")}
            resizeMode="contain"
            style={styles.image3}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  stack: {
    position: "relative"
  },
  image: {
    width: 827,
    height: 827,
    position: "absolute",
    left: 0,
    top: 0
  },
  materialHeader1: {
    top: 7,
    width: 375,
    height: 60,
    position: "absolute",
    left: 0
  },
  image2: {
    top: 0,
    width: 126,
    height: 77,
    position: "absolute",
    left: 125
  },
  usersheader: {
    width: 375,
    height: 40,
    marginTop: 13,
    marginLeft: 226
  },
  user1Info: {
    width: 375,
    height: 40,
    marginTop: 2,
    marginLeft: 226
  },
  user2Info: {
    width: 375,
    height: 40,
    marginTop: 1,
    marginLeft: 226
  },
  user3Info: {
    width: 375,
    height: 40,
    marginTop: 1,
    marginLeft: 226
  },
  user4Info: {
    width: 375,
    height: 40,
    marginTop: 1,
    marginLeft: 226
  },
  user5Info: {
    width: 375,
    height: 40,
    marginTop: 1,
    marginLeft: 226
  },
  materialButtonLight: {
    width: 109,
    height: 36,
    marginTop: 19,
    marginLeft: 364
  },
  footerNav: {
    width: 375,
    height: 56,
    marginTop: 260,
    marginLeft: 226
  },
  image3: {
    top: 682,
    left: 331,
    width: 175,
    height: 175,
    position: "absolute"
  }
});
