import React, { Component } from "react";
import { StyleSheet, View, Image, ImageBackground, Text } from "react-native";
import MaterialHeader1 from "../components/MaterialHeader1";
import Mypicture from "../components/Mypicture";
import MaterialButtonPurple from "../components/MaterialButtonPurple";
import Firstname from "../components/Firstname";
import NameFirst from "../components/NameFirst";
import LastName from "../components/LastName";
import Jobtitle from "../components/Jobtitle";
import NameLast from "../components/NameLast";
import Emailaddress from "../components/Emailaddress";
import Cellphone from "../components/Cellphone";
import Website from "../components/Website";
import Officephone from "../components/Officephone";
import Faxnumber from "../components/Faxnumber";
import MaterialUnderlineTextbox9 from "../components/MaterialUnderlineTextbox9";
import Teamname from "../components/Teamname";
import SaveButton from "../components/SaveButton";
import FooterNav from "../components/FooterNav";

export default class MyProfile extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View
          style={[
            styles.stack,
            {
              marginTop: -7,
              marginLeft: -226,
              height: 857,
              width: 827
            }
          ]}
        >
          <ImageBackground
            source={require("../assets/images/background.png")}
            resizeMode="contain"
            style={styles.image2}
          >
            <View
              style={[
                styles.stack,
                {
                  marginTop: 40,
                  marginLeft: 226,
                  height: 77,
                  width: 375
                }
              ]}
            >
              <MaterialHeader1 style={styles.materialHeader1} />
              <Image
                source={require("../assets/images/digital-catalog-logo.png")}
                resizeMode="contain"
                style={styles.image}
              />
            </View>
            <View
              style={[
                styles.stack,
                {
                  marginTop: 13,
                  marginLeft: 355,
                  height: 20,
                  width: 117
                }
              ]}
            >
              <Text style={styles.text11}>MY PROFILE</Text>
              <Text style={styles.text12}>MY PROFILE</Text>
            </View>
            <View
              style={[
                styles.row,
                {
                  marginTop: 17,
                  marginLeft: 247,
                  marginRight: 259,
                  height: 75
                }
              ]}
            >
              <Mypicture style={styles.mypicture} />
              <MaterialButtonPurple style={styles.materialButtonPurple} />
            </View>
            <View
              style={[
                styles.stack,
                {
                  marginTop: 12,
                  marginLeft: 250,
                  height: 72,
                  width: 331
                }
              ]}
            >
              <View
                style={[
                  styles.stack,
                  {
                    top: 0,
                    left: 0,
                    height: 68,
                    width: 331,
                    position: "absolute"
                  }
                ]}
              >
                <Firstname style={styles.firstname} />
                <NameFirst style={styles.nameFirst} />
              </View>
              <LastName style={styles.lastName} />
            </View>
            <View
              style={[
                styles.stack,
                {
                  marginTop: 4,
                  marginLeft: 256,
                  height: 42,
                  width: 325
                }
              ]}
            >
              <Jobtitle style={styles.jobtitle} />
              <NameLast style={styles.nameLast} />
            </View>
            <Emailaddress style={styles.emailaddress} />
            <Cellphone style={styles.cellphone} />
            <Website style={styles.website} />
            <Officephone style={styles.officephone} />
            <Faxnumber style={styles.faxnumber} />
            <View
              style={[
                styles.stack,
                {
                  marginTop: 3,
                  marginLeft: 253,
                  height: 39,
                  width: 328
                }
              ]}
            >
              <View
                style={[
                  styles.stack,
                  {
                    top: 0,
                    left: 0,
                    height: 39,
                    width: 328,
                    position: "absolute"
                  }
                ]}
              >
                <MaterialUnderlineTextbox9
                  style={styles.materialUnderlineTextbox9}
                />
                <Teamname style={styles.teamname} />
              </View>
              <Text style={styles.text10}>Team Name</Text>
            </View>
            <SaveButton style={styles.materialButtonPurple1} />
            <FooterNav style={styles.materialBasicFooter1} />
          </ImageBackground>
          <ImageBackground
            source={require("../assets/images/powerbycatalogos_logo.png")}
            resizeMode="contain"
            style={styles.image4}
          >
            <View style={styles.rect} />
          </ImageBackground>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  stack: {
    position: "relative"
  },
  image2: {
    top: 0,
    left: 0,
    width: 827,
    height: 827,
    position: "absolute"
  },
  materialHeader1: {
    top: 7,
    left: 0,
    width: 375,
    height: 60,
    position: "absolute"
  },
  image: {
    top: 0,
    width: 126,
    height: 77,
    position: "absolute",
    left: 125
  },
  text11: {
    top: 0,
    left: 0,
    color: "rgba(0,0,0,1)",
    position: "absolute",
    fontSize: 20
  },
  text12: {
    top: 0,
    left: 0,
    color: "rgba(0,0,0,1)",
    position: "absolute",
    fontSize: 20
  },
  row: {
    flexDirection: "row"
  },
  mypicture: {
    width: 171,
    height: 75
  },
  materialButtonPurple: {
    width: 134,
    height: 28,
    marginLeft: 16,
    marginTop: 25
  },
  firstname: {
    top: 0,
    left: 0,
    width: 330,
    height: 33,
    position: "absolute"
  },
  nameFirst: {
    top: 29,
    left: 6,
    width: 325,
    height: 39,
    position: "absolute"
  },
  lastName: {
    top: 40,
    left: 2,
    width: 328,
    height: 32,
    position: "absolute"
  },
  jobtitle: {
    top: 3,
    left: 17,
    width: 307,
    height: 39,
    position: "absolute"
  },
  nameLast: {
    top: 0,
    left: 0,
    width: 325,
    height: 39,
    position: "absolute"
  },
  emailaddress: {
    width: 294,
    height: 39,
    marginTop: 3,
    marginLeft: 286
  },
  cellphone: {
    width: 330,
    height: 39,
    marginTop: 5,
    marginLeft: 250
  },
  website: {
    width: 335,
    height: 39,
    marginTop: 3,
    marginLeft: 245
  },
  officephone: {
    width: 338,
    height: 39,
    marginTop: 4,
    marginLeft: 242
  },
  faxnumber: {
    width: 330,
    height: 39,
    marginTop: 4,
    marginLeft: 250
  },
  materialUnderlineTextbox9: {
    top: 0,
    left: 91,
    width: 237,
    height: 39,
    position: "absolute"
  },
  teamname: {
    top: 0,
    left: 0,
    width: 327,
    height: 39,
    position: "absolute"
  },
  text10: {
    top: 13,
    left: 0,
    color: "#121212",
    position: "absolute"
  },
  materialButtonPurple1: {
    width: 100,
    height: 36,
    marginTop: 16,
    marginLeft: 344
  },
  materialBasicFooter1: {
    width: 375,
    height: 56,
    marginTop: 11,
    marginLeft: 226
  },
  image4: {
    top: 682,
    left: 226,
    width: 175,
    height: 175,
    position: "absolute"
  },
  rect: {
    width: 79,
    height: 56,
    backgroundColor: "rgba(255,255,255,1)",
    opacity: 0.51,
    marginTop: 9,
    marginLeft: 7
  }
});
