import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import Svg, { Ellipse } from "react-native-svg";
import Icon from "react-native-vector-icons/Entypo";

export default class Mypicture extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View
          style={[
            styles.row,
            {
              height: 75,
              flex: 1
            }
          ]}
        >
          <Text style={styles.text5}>My Picture *</Text>
          <View
            style={[
              styles.stack,
              {
                marginLeft: 17,
                height: 75,
                width: 75
              }
            ]}
          >
            <Svg viewBox="0 0 74.78 74.78" style={styles.ellipse}>
              <Ellipse
                strokeWidth={1}
                fill="rgba(255,255,255,1)"
                stroke="rgba(155,155,155,1)"
                cx={37.38999938964844}
                cy={37.38999938964844}
                rx={36.88999938964844}
                ry={36.88999938964844}
              />
            </Svg>
            <Icon name="user" style={styles.icon} />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row"
  },
  row: {
    flexDirection: "row"
  },
  text5: {
    color: "#121212",
    marginTop: 29
  },
  stack: {
    position: "relative"
  },
  ellipse: {
    top: 0,
    left: 0,
    width: 75,
    height: 75,
    position: "absolute"
  },
  icon: {
    top: 17,
    left: 18,
    position: "absolute",
    color: "rgba(155,155,155,1)",
    fontSize: 40
  }
});
