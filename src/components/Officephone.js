import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import MaterialUnderlineTextbox7 from "./MaterialUnderlineTextbox7";

export default class Officephone extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View
          style={[
            styles.row,
            {
              height: 39,
              flex: 1
            }
          ]}
        >
          <Text style={styles.text8}>Direct Office{"\n"}Phone</Text>
          <MaterialUnderlineTextbox7 style={styles.materialUnderlineTextbox7} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row"
  },
  row: {
    flexDirection: "row"
  },
  text8: {
    color: "#121212",
    textAlign: "right",
    marginTop: 1
  },
  materialUnderlineTextbox7: {
    width: 237,
    height: 39,
    marginLeft: 15
  }
});
