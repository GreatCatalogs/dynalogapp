import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import MaterialUnderlineTextbox1 from "./MaterialUnderlineTextbox1";

export default class NewleadFirstName extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View
          style={[
            styles.row,
            {
              height: 33,
              flex: 1
            }
          ]}
        >
          <Text style={styles.text}>First Name</Text>
          <MaterialUnderlineTextbox1 style={styles.materialUnderlineTextbox1} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row"
  },
  row: {
    flexDirection: "row"
  },
  text: {
    color: "rgba(0,0,0,1)",
    marginTop: 9
  },
  materialUnderlineTextbox1: {
    width: 237,
    height: 33,
    marginLeft: 8
  }
});
