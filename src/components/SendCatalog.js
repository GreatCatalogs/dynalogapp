import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

export default class SendCatalog extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View
          style={[
            styles.row,
            {
              height: 30,
              flex: 1
            }
          ]}
        >
          <Icon name="animation-outline" style={styles.icon3} />
          <Text style={styles.text}>SEND A CATALOG</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row"
  },
  row: {
    flexDirection: "row"
  },
  icon3: {
    color: "rgba(136,136,136,1)",
    fontSize: 30
  },
  text: {
    color: "rgba(136,136,136,1)",
    fontSize: 18,
    marginLeft: 8,
    marginTop: 6
  }
});
