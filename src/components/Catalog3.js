import React, { Component } from "react";
import { StyleSheet, View, Image } from "react-native";
import MaterialButtonPurple2 from "./MaterialButtonPurple2";

export default class Catalog3 extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View
          style={[
            styles.stack,
            {
              height: 200,
              width: 168
            }
          ]}
        >
          <Image
            source={require("../assets/images/catalog-sample3.png")}
            resizeMode="contain"
            style={styles.image6}
          />
          <MaterialButtonPurple2 style={styles.materialButtonPurple23} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  stack: {
    position: "relative"
  },
  image6: {
    top: 0,
    left: 0,
    width: 168,
    height: 168,
    position: "absolute"
  },
  materialButtonPurple23: {
    top: 168,
    left: 28,
    width: 112,
    height: 32,
    position: "absolute"
  }
});
