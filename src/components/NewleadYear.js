import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import MaterialUnderlineTextbox1 from "./MaterialUnderlineTextbox1";

export default class NewleadYear extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View
          style={[
            styles.row,
            {
              height: 33,
              flex: 1
            }
          ]}
        >
          <Text style={styles.text5}>Year</Text>
          <MaterialUnderlineTextbox1
            style={styles.materialUnderlineTextbox15}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row"
  },
  row: {
    flexDirection: "row"
  },
  text5: {
    color: "rgba(0,0,0,1)",
    marginTop: 9
  },
  materialUnderlineTextbox15: {
    width: 237,
    height: 33,
    marginLeft: 9
  }
});
