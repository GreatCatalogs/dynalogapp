import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import MaterialUnderlineTextbox10 from "./MaterialUnderlineTextbox10";

export default class NameLast extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View
          style={[
            styles.row,
            {
              height: 39,
              flex: 1
            }
          ]}
        >
          <Text style={styles.text13}>Last Name *</Text>
          <MaterialUnderlineTextbox10
            style={styles.materialUnderlineTextbox10}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row"
  },
  row: {
    flexDirection: "row"
  },
  text13: {
    color: "#121212",
    fontFamily: "roboto-regular",
    marginTop: 13
  },
  materialUnderlineTextbox10: {
    width: 237,
    height: 39,
    marginLeft: 11
  }
});
