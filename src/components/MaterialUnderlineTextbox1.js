import React, { Component } from "react";
import { StyleSheet, View, TextInput } from "react-native";

export default class MaterialUnderlineTextbox1 extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <TextInput placeholder="" editable={false} style={styles.inputStyle} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "transparent",
    flexDirection: "row",
    alignItems: "center",
    borderColor: "#D9D5DC",
    borderBottomWidth: 1
  },
  inputStyle: {
    flex: 1,
    backgroundColor: "rgba(255,255,255,1)",
    color: "#000",
    alignSelf: "stretch",
    paddingTop: 16,
    paddingRight: 5,
    paddingBottom: 8,
    borderRadius: 10,
    fontSize: 16,
    lineHeight: 16
  }
});
