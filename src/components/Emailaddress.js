import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import MaterialUnderlineTextbox4 from "./MaterialUnderlineTextbox4";

export default class Emailaddress extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View
          style={[
            styles.row,
            {
              height: 39,
              flex: 1
            }
          ]}
        >
          <Text style={styles.text4}>Email *</Text>
          <MaterialUnderlineTextbox4 style={styles.materialUnderlineTextbox4} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row"
  },
  row: {
    flexDirection: "row"
  },
  text4: {
    color: "#121212",
    marginTop: 13
  },
  materialUnderlineTextbox4: {
    width: 237,
    height: 39,
    marginLeft: 13
  }
});
