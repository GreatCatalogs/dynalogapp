import React, { Component } from "react";
import { StyleSheet, View } from "react-native";

export default class Contact6 extends Component {
  render() {
    return <View style={[styles.container, this.props.style]} />;
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "rgba(255,255,255,1)"
  }
});
