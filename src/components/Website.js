import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import MaterialUnderlineTextbox6 from "./MaterialUnderlineTextbox6";

export default class Website extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View
          style={[
            styles.row,
            {
              height: 39,
              flex: 1
            }
          ]}
        >
          <Text style={styles.text7}>Personal {"\n"}Website URL</Text>
          <MaterialUnderlineTextbox6 style={styles.materialUnderlineTextbox6} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row"
  },
  row: {
    flexDirection: "row"
  },
  text7: {
    color: "#121212",
    textAlign: "right",
    marginTop: 6
  },
  materialUnderlineTextbox6: {
    width: 237,
    height: 39,
    marginLeft: 15
  }
});
