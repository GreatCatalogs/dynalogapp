import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import MaterialCheckbox5 from "./MaterialCheckbox5";

export default class Contact5 extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View style={styles.rect5}>
          <View
            style={[
              styles.row,
              {
                marginRight: 21,
                marginLeft: 17,
                marginTop: 5,
                height: 40,
                flex: 1
              }
            ]}
          >
            <Icon name="plus-circle" style={styles.icon4} />
            <Text style={styles.text4}>Contact 5</Text>
            <MaterialCheckbox5 style={styles.materialCheckbox5} />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  rect5: {
    width: 375,
    height: 51,
    backgroundColor: "rgba(255,255,255,1)",
    flexDirection: "row"
  },
  row: {
    flexDirection: "row"
  },
  icon4: {
    color: "grey",
    fontSize: 30,
    height: 30,
    width: 30,
    marginTop: 5
  },
  text4: {
    color: "rgba(136,136,136,1)",
    fontSize: 14,
    fontFamily: "roboto-regular",
    marginLeft: 13,
    marginTop: 13
  },
  materialCheckbox5: {
    width: 40,
    height: 40,
    marginLeft: 194
  }
});
