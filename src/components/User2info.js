import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

export default class User2info extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View style={styles.rect2}>
          <View
            style={[
              styles.row,
              {
                marginRight: 19,
                marginLeft: 19,
                marginTop: 4,
                height: 30,
                flex: 1
              }
            ]}
          >
            <Text style={styles.text2}>User 2 Name</Text>
            <Icon name="arrow-right-bold-circle" style={styles.icon2} />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  rect2: {
    width: 375,
    height: 40,
    backgroundColor: "rgba(255,255,255,1)",
    flexDirection: "row"
  },
  row: {
    flexDirection: "row"
  },
  text2: {
    color: "rgba(136,136,136,1)",
    fontSize: 12,
    fontFamily: "roboto-regular",
    marginTop: 10
  },
  icon2: {
    color: "grey",
    fontSize: 30,
    height: 30,
    width: 30,
    marginLeft: 238
  }
});
