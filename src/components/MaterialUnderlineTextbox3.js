import React, { Component } from "react";
import { StyleSheet, View, TextInput } from "react-native";

export default class MaterialUnderlineTextbox3 extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <TextInput placeholder="" editable={false} style={styles.inputStyle} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "transparent",
    flexDirection: "row",
    alignItems: "center"
  },
  inputStyle: {
    height: 43,
    flex: 1,
    backgroundColor: "rgba(255,255,255,1)",
    color: "#000",
    paddingTop: 0,
    paddingRight: 0,
    paddingBottom: 0,
    borderRadius: 10,
    fontSize: 16,
    lineHeight: 16
  }
});
