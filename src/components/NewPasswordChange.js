import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import NewPassword from "./NewPassword";

export default class NewPasswordChange extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View
          style={[
            styles.stack,
            {
              height: 61,
              width: 311
            }
          ]}
        >
          <NewPassword style={styles.materialUnderlineTextbox12} />
          <Text style={styles.text}>Password*</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  stack: {
    position: "relative"
  },
  materialUnderlineTextbox12: {
    top: 18,
    left: 0,
    width: 311,
    height: 43,
    position: "absolute"
  },
  text: {
    top: 0,
    left: 0,
    color: "rgba(74,74,74,1)",
    position: "absolute",
    fontSize: 14,
    fontFamily: "roboto-regular",
    lineHeight: 18
  }
});
