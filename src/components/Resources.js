import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

export default class Resources extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View style={styles.rect5}>
          <View
            style={[
              styles.row,
              {
                marginRight: 38,
                marginLeft: 32,
                marginTop: 11,
                height: 30,
                flex: 1
              }
            ]}
          >
            <Text style={styles.text4}>Resources</Text>
            <Icon name="folder-multiple-image" style={styles.icon6} />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  rect5: {
    width: 375,
    height: 51,
    backgroundColor: "rgba(62,145,188,1)",
    flexDirection: "row"
  },
  row: {
    flexDirection: "row"
  },
  text4: {
    color: "rgba(255,255,255,1)",
    fontSize: 18,
    fontFamily: "roboto-700",
    marginTop: 6
  },
  icon6: {
    color: "rgba(255,255,255,1)",
    fontSize: 30,
    height: 30,
    width: 22,
    marginLeft: 198
  }
});
