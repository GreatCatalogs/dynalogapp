import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

export default class Logout extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View
          style={[
            styles.row,
            {
              height: 30,
              flex: 1
            }
          ]}
        >
          <Text style={styles.text2}>Logout</Text>
          <Icon name="logout-variant" style={styles.icon4} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row"
  },
  row: {
    flexDirection: "row"
  },
  text2: {
    color: "rgba(255,255,255,1)",
    fontSize: 18,
    fontFamily: "roboto-700",
    marginTop: 6
  },
  icon4: {
    color: "rgba(255,255,255,1)",
    fontSize: 30,
    marginLeft: 10
  }
});
