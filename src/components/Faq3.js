import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

export default class Faq3 extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View style={styles.rect4}>
          <View
            style={[
              styles.row,
              {
                marginRight: 84,
                marginLeft: 17,
                marginTop: 9,
                height: 20,
                flex: 1
              }
            ]}
          >
            <Icon name="arrow-right-bold-circle" style={styles.icon4} />
            <Text style={styles.text4}>
              How often is my Personal Dynalog updated?
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  rect4: {
    width: 375,
    height: 36,
    backgroundColor: "rgba(255,255,255,1)",
    flexDirection: "row"
  },
  row: {
    flexDirection: "row"
  },
  icon4: {
    color: "grey",
    fontSize: 20,
    height: 20,
    width: 20
  },
  text4: {
    color: "rgba(74,74,74,1)",
    fontSize: 12,
    fontFamily: "roboto-regular",
    marginLeft: 5,
    marginTop: 2
  }
});
