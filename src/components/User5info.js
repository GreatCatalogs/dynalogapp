import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

export default class User5info extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View style={styles.rect5}>
          <View
            style={[
              styles.row,
              {
                marginRight: 19,
                marginLeft: 19,
                marginTop: 4,
                height: 30,
                flex: 1
              }
            ]}
          >
            <Text style={styles.text5}>User 5 Name</Text>
            <Icon name="arrow-right-bold-circle" style={styles.icon5} />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  rect5: {
    width: 375,
    height: 40,
    backgroundColor: "rgba(255,255,255,1)",
    flexDirection: "row"
  },
  row: {
    flexDirection: "row"
  },
  text5: {
    color: "rgba(136,136,136,1)",
    fontSize: 12,
    fontFamily: "roboto-regular",
    marginTop: 9
  },
  icon5: {
    color: "grey",
    fontSize: 30,
    height: 30,
    width: 30,
    marginLeft: 207
  }
});
