import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";

export default class Firstname extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <Text style={styles.text}>First name *</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  text: {
    color: "rgba(0,0,0,1)",
    fontSize: 14,
    marginTop: 9
  }
});
