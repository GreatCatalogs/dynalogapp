import React, { Component } from "react";
import { StyleSheet, View } from "react-native";
import Contact1 from "./Contact1";

export default class Contact1new extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View style={styles.rect}>
          <Contact1 style={styles.contact1} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  rect: {
    width: 375,
    height: 51,
    backgroundColor: "rgba(255,255,255,1)"
  },
  contact1: {
    width: 336,
    height: 40,
    marginTop: 8,
    marginLeft: 17
  }
});
