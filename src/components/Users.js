import React, { Component } from "react";
import { StyleSheet, View } from "react-native";

export default class Users extends Component {
  render() {
    return <View style={[styles.container, this.props.style]} />;
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "rgba(62,145,188,1)"
  }
});
