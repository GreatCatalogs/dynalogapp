import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

export default class ViewMyCarCatalog extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View style={styles.rect2}>
          <View
            style={[
              styles.row,
              {
                marginRight: 111,
                marginLeft: 17,
                height: 40,
                flex: 1
              }
            ]}
          >
            <Icon name="keyboard-backspace" style={styles.icon} />
            <Text style={styles.text}>VIEW MY CAR CATALOG</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  rect2: {
    width: 375,
    height: 40,
    backgroundColor: "rgba(230, 230, 230,1)",
    flexDirection: "row"
  },
  row: {
    flexDirection: "row"
  },
  icon: {
    color: "grey",
    fontSize: 40,
    height: 40,
    width: 29
  },
  text: {
    color: "rgba(136,136,136,1)",
    fontSize: 18,
    fontFamily: "roboto-regular",
    marginLeft: 20,
    marginTop: 11
  }
});
