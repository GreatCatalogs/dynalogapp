import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

export default class Faq7 extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View style={styles.rect8}>
          <View
            style={[
              styles.row,
              {
                marginRight: 18,
                marginLeft: 17,
                marginTop: 7,
                height: 24,
                flex: 1
              }
            ]}
          >
            <Icon name="arrow-right-bold-circle" style={styles.icon8} />
            <Text style={styles.text8}>
              Do my prospects/clients have to download this app to see my
              Personal Dynalog?
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  rect8: {
    width: 375,
    height: 36,
    backgroundColor: "rgba(255,255,255,1)",
    flexDirection: "row"
  },
  row: {
    flexDirection: "row"
  },
  icon8: {
    color: "grey",
    fontSize: 20,
    height: 20,
    width: 20,
    marginTop: 2
  },
  text8: {
    color: "rgba(74,74,74,1)",
    fontSize: 12,
    fontFamily: "roboto-regular",
    marginLeft: 5
  }
});
