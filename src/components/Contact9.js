import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import MaterialCheckbox9 from "./MaterialCheckbox9";

export default class Contact9 extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View style={styles.rect9}>
          <View
            style={[
              styles.row,
              {
                marginRight: 18,
                marginLeft: 17,
                marginTop: 6,
                height: 40,
                flex: 1
              }
            ]}
          >
            <Icon name="plus-circle" style={styles.icon8} />
            <Text style={styles.text8}>Contact 9</Text>
            <MaterialCheckbox9 style={styles.materialCheckbox9} />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  rect9: {
    width: 375,
    height: 51,
    backgroundColor: "rgba(255,255,255,1)",
    flexDirection: "row"
  },
  row: {
    flexDirection: "row"
  },
  icon8: {
    color: "grey",
    fontSize: 30,
    height: 30,
    width: 30,
    marginTop: 4
  },
  text8: {
    color: "rgba(136,136,136,1)",
    fontSize: 14,
    fontFamily: "roboto-regular",
    marginLeft: 13,
    marginTop: 13
  },
  materialCheckbox9: {
    width: 40,
    height: 40,
    marginLeft: 197
  }
});
