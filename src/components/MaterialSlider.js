import React, { Component } from "react";
import { StyleSheet, View, Slider } from "react-native";

export default class MaterialSlider extends Component {
  render() {
    return (
      <View style={[styles.root, this.props.style]}>
        <Slider
          minimumValue={0}
          maximumValue={100}
          thumbTintColor="#3F51B5"
          minimumTrackTintColor="#3F51B5"
          maximumTrackTintColor="#9E9E9E"
          style={styles.slider}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: "transparent",
    justifyContent: "center"
  },
  slider: {}
});
