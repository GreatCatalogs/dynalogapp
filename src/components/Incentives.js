import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

export default class Incentives extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View style={styles.rect6}>
          <View
            style={[
              styles.row,
              {
                marginRight: 38,
                marginLeft: 32,
                marginTop: 11,
                height: 30,
                flex: 1
              }
            ]}
          >
            <Text style={styles.text5}>Incentives</Text>
            <Icon name="cash" style={styles.icon7} />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  rect6: {
    width: 375,
    height: 51,
    backgroundColor: "rgba(62,145,188,1)",
    flexDirection: "row"
  },
  row: {
    flexDirection: "row"
  },
  text5: {
    color: "rgba(255,255,255,1)",
    fontSize: 18,
    fontFamily: "roboto-700",
    marginTop: 6
  },
  icon7: {
    color: "rgba(255,255,255,1)",
    fontSize: 30,
    height: 30,
    width: 22,
    marginLeft: 201
  }
});
