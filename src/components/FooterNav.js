import React, { Component } from "react";
import { StyleSheet, View, TouchableOpacity, Text } from "react-native";
import MaterialCommunityIconsIcon from "react-native-vector-icons/MaterialCommunityIcons";
import IoniconsIcon from "react-native-vector-icons/Ionicons";
import EntypoIcon from "react-native-vector-icons/Entypo";

export default class FooterNav extends Component {
  render() {
    return (
      <View style={[styles.root, this.props.style]}>
        <View
          style={[
            styles.stack,
            {
              marginTop: -17,
              height: 90,
              width: 375
            }
          ]}
        >
          <TouchableOpacity style={styles.btnWrapper1}>
            <Text style={styles.btn1Text}>HOME</Text>
            <MaterialCommunityIconsIcon name="home" style={styles.icon4} />
          </TouchableOpacity>
          <TouchableOpacity style={styles.activebtnWrapper}>
            <Text style={styles.activeText}>LEADS</Text>
            <IoniconsIcon name="md-people" style={styles.icon5} />
          </TouchableOpacity>
          <TouchableOpacity style={styles.btnWrapper2}>
            <Text style={styles.btn2Text}>ADD RESOURCES</Text>
            <EntypoIcon name="circle-with-plus" style={styles.icon6} />
          </TouchableOpacity>
          <TouchableOpacity style={styles.btnWrapper3}>
            <Text style={styles.btn3Text}>VIEW CATALOGS</Text>
            <IoniconsIcon name="ios-eye" style={styles.icon7} />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: "rgba(38,98,128,1)",
    elevation: 3,
    shadowOffset: {
      height: -2,
      width: 0
    },
    shadowColor: "#111",
    shadowOpacity: 0.2,
    shadowRadius: 1.2
  },
  stack: {
    position: "relative"
  },
  btnWrapper1: {
    top: 0,
    left: 0,
    width: 94,
    height: 90,
    flex: 1,
    position: "absolute",
    alignItems: "center",
    paddingTop: 25,
    paddingBottom: 25,
    maxWidth: 168,
    paddingHorizontal: 12
  },
  btn1Text: {
    color: "#FFFFFF",
    opacity: 0.8,
    paddingTop: 33,
    fontSize: 7
  },
  icon4: {
    top: 21,
    left: 29,
    position: "absolute",
    color: "rgba(255,255,255,1)",
    fontSize: 35,
    opacity: 1
  },
  activebtnWrapper: {
    top: 15,
    left: 94,
    width: 94,
    height: 61,
    flex: 1,
    position: "absolute",
    alignItems: "center",
    opacity: 1,
    paddingTop: 6,
    paddingBottom: 10,
    maxWidth: 168,
    paddingHorizontal: 12
  },
  activeText: {
    color: "#FFFFFF",
    opacity: 1,
    paddingTop: 38,
    fontSize: 7,
    fontFamily: "arial-regular"
  },
  icon5: {
    top: 9,
    left: 32,
    position: "absolute",
    color: "rgba(255,255,255,1)",
    fontSize: 35
  },
  btnWrapper2: {
    top: 18,
    left: 188,
    width: 94,
    height: 55,
    flex: 1,
    position: "absolute",
    alignItems: "center",
    paddingTop: 8,
    paddingBottom: 6,
    maxWidth: 168,
    paddingHorizontal: 12
  },
  btn2Text: {
    color: "#FFFFFF",
    opacity: 0.8,
    paddingTop: 34,
    fontSize: 7
  },
  icon6: {
    top: 6,
    left: 31,
    position: "absolute",
    color: "rgba(255,255,255,1)",
    fontSize: 32
  },
  btnWrapper3: {
    top: 19,
    left: 281,
    width: 94,
    height: 53,
    flex: 1,
    position: "absolute",
    alignItems: "center",
    paddingTop: 8,
    paddingBottom: 6,
    maxWidth: 168,
    paddingHorizontal: 12
  },
  btn3Text: {
    color: "#FFFFFF",
    opacity: 0.8,
    paddingTop: 32,
    fontSize: 7,
    fontFamily: "arial-regular"
  },
  icon7: {
    top: 5,
    left: 28,
    position: "absolute",
    color: "rgba(255,255,255,1)",
    fontSize: 35
  }
});
