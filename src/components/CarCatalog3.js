import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import CarCatolog2 from "./CarCatolog2";
import TaptoViewButton from "./TaptoViewButton";
import SocialMediaButtons from "./SocialMediaButtons";

export default class CarCatalog3 extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <CarCatolog2 style={styles.carCatolog2} />
        <View style={styles.rect5}>
          <TaptoViewButton style={styles.materialButtonLight83} />
          <SocialMediaButtons style={styles.socialMediaButtons3} />
          <Text style={styles.text4}>
            You can also link your Dynalog from anywhere you want!{"\n"}(hold to
            copy the URL below){"\n"}
            https://dynalog-qa.catalogs.com/g3444/toyota-inventory-sample?merchantID=1557&amp;padUID=1266
          </Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  carCatolog2: {
    width: 375,
    height: 170
  },
  rect5: {
    width: 375,
    height: 170,
    backgroundColor: "rgba(192,192,192,1)",
    marginTop: 2
  },
  materialButtonLight83: {
    width: 171,
    height: 36,
    marginTop: 45,
    marginLeft: 170
  },
  socialMediaButtons3: {
    width: 93,
    height: 30,
    marginTop: 10,
    marginLeft: 209
  },
  text4: {
    color: "rgba(0,0,0,1)",
    fontSize: 7,
    fontFamily: "roboto-700",
    marginTop: 11,
    marginLeft: 154
  }
});
