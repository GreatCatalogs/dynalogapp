import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import MaterialCheckbox2 from "./MaterialCheckbox2";

export default class Contact2 extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View style={styles.rect2}>
          <View
            style={[
              styles.row,
              {
                marginRight: 22,
                marginLeft: 17,
                marginTop: 5,
                height: 40,
                flex: 1
              }
            ]}
          >
            <Icon name="plus-circle" style={styles.icon} />
            <Text style={styles.text}>Contact 2</Text>
            <MaterialCheckbox2 style={styles.materialCheckbox2} />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  rect2: {
    width: 375,
    height: 51,
    backgroundColor: "rgba(255,255,255,1)",
    flexDirection: "row"
  },
  row: {
    flexDirection: "row"
  },
  icon: {
    color: "grey",
    fontSize: 30,
    height: 30,
    width: 30,
    marginTop: 5
  },
  text: {
    color: "rgba(136,136,136,1)",
    fontSize: 14,
    fontFamily: "roboto-regular",
    marginLeft: 13,
    marginTop: 13
  },
  materialCheckbox2: {
    width: 40,
    height: 40,
    marginLeft: 190
  }
});
