import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import Icon from "react-native-vector-icons/Entypo";

export default class SelectResource extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View style={styles.rect}>
          <View
            style={[
              styles.row,
              {
                marginRight: 13,
                marginLeft: 19,
                marginTop: 4,
                height: 27,
                flex: 1
              }
            ]}
          >
            <Icon name="folder-images" style={styles.icon} />
            <Text style={styles.text2}>
              Please select the Resource(s) you want to send.
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  rect: {
    width: 375,
    height: 35,
    backgroundColor: "rgba(232,232,232,1)",
    flexDirection: "row"
  },
  row: {
    flexDirection: "row"
  },
  icon: {
    color: "rgba(136,136,136,1)",
    fontSize: 27,
    height: 27,
    width: 27
  },
  text2: {
    color: "rgba(74,74,74,1)",
    fontSize: 14,
    marginLeft: 6,
    marginTop: 7
  }
});
