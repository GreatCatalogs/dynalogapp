import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import MaterialUnderlineTextbox1 from "./MaterialUnderlineTextbox1";

export default class NewleadLastName extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View
          style={[
            styles.row,
            {
              height: 33,
              flex: 1
            }
          ]}
        >
          <Text style={styles.text2}>Last name</Text>
          <MaterialUnderlineTextbox1
            style={styles.materialUnderlineTextbox12}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row"
  },
  row: {
    flexDirection: "row"
  },
  text2: {
    color: "rgba(0,0,0,1)",
    marginTop: 9
  },
  materialUnderlineTextbox12: {
    width: 237,
    height: 33,
    marginLeft: 8
  }
});
