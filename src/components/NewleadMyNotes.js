import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import MaterialUnderlineTextbox1 from "./MaterialUnderlineTextbox1";

export default class NewleadMyNotes extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View
          style={[
            styles.row,
            {
              height: 64,
              flex: 1
            }
          ]}
        >
          <View
            style={[
              styles.stack,
              {
                marginTop: 18,
                height: 22,
                width: 90
              }
            ]}
          >
            <Text style={styles.text9}>My Notes</Text>
            <Text style={styles.text10}>(not sent to customer)</Text>
          </View>
          <MaterialUnderlineTextbox1
            style={styles.materialUnderlineTextbox19}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row"
  },
  row: {
    flexDirection: "row"
  },
  stack: {
    position: "relative"
  },
  text9: {
    top: 0,
    left: 28,
    color: "rgba(0,0,0,1)",
    position: "absolute"
  },
  text10: {
    top: 14,
    left: 0,
    color: "rgba(0,0,0,1)",
    position: "absolute",
    fontSize: 8
  },
  materialUnderlineTextbox19: {
    width: 237,
    height: 64,
    marginLeft: 5
  }
});
