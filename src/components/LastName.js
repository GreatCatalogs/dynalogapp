import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";

export default class LastName extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <Text style={styles.text2}>Last name *</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  text2: {
    color: "#121212",
    marginTop: 9
  }
});
