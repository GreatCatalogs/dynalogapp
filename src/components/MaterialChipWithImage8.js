import React, { Component } from "react";
import { StyleSheet, View, Text, Image } from "react-native";

export default class MaterialChipWithImage8 extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <Text style={styles.chipText}>SET 10 APTS IN 3 DAYS AND WIN</Text>
        <Image
          source={require("../assets/images/digital-catalog-logo1.png")}
          resizeMode="contain"
          style={styles.image}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(255,255,255,1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    borderRadius: 50
  },
  chipText: {
    color: "rgba(0,0,0,0.87)",
    paddingRight: 12,
    paddingLeft: 34,
    fontSize: 13
  },
  image: {
    top: -21,
    left: -19,
    width: 78,
    height: 78,
    position: "absolute"
  }
});
