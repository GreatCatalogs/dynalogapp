import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import MaterialCheckbox from "./MaterialCheckbox";

export default class Resource5 extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View
          style={[
            styles.stack,
            {
              height: 41,
              width: 161
            }
          ]}
        >
          <MaterialCheckbox style={styles.materialCheckbox5} />
          <Text style={styles.text8}>Free $25 Gift Card</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  stack: {
    position: "relative"
  },
  materialCheckbox5: {
    top: 0,
    left: 0,
    width: 41,
    height: 41,
    position: "absolute"
  },
  text8: {
    top: 14,
    left: 41,
    color: "rgba(74,74,74,1)",
    position: "absolute",
    fontSize: 14
  }
});
