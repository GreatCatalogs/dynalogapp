import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";

export default class Jobtitle extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <Text style={styles.text3}>Job Title</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  text3: {
    color: "#121212",
    marginTop: 13
  }
});
