import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

export default class MyLeads extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View style={styles.rect4}>
          <View
            style={[
              styles.row,
              {
                marginRight: 261,
                marginLeft: 14,
                height: 40,
                flex: 1
              }
            ]}
          >
            <Icon name="keyboard-backspace" style={styles.icon} />
            <Text style={styles.text2}>LEADS</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  rect4: {
    width: 375,
    height: 40,
    backgroundColor: "rgba(230, 230, 230,1)",
    flexDirection: "row"
  },
  row: {
    flexDirection: "row"
  },
  icon: {
    color: "grey",
    fontSize: 40,
    height: 40,
    width: 40
  },
  text2: {
    color: "rgba(136,136,136,1)",
    fontSize: 18,
    fontFamily: "arial-regular",
    marginLeft: 5,
    marginTop: 11
  }
});
