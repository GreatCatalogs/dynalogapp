import React, { Component } from "react";
import { StyleSheet, View, Image, Text } from "react-native";
import TaptoViewButton from "./TaptoViewButton";
import SocialMediaButtons from "./SocialMediaButtons";

export default class CarCatalog1 extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View style={styles.rect3}>
          <View
            style={[
              styles.stack,
              {
                height: 170,
                width: 365
              }
            ]}
          >
            <View
              style={[
                styles.stack,
                {
                  top: 0,
                  left: 0,
                  height: 170,
                  width: 365,
                  position: "absolute"
                }
              ]}
            >
              <Image
                source={require("../assets/images/catalog-sample11.png")}
                resizeMode="contain"
                style={styles.image4}
              />
              <TaptoViewButton style={styles.materialButtonLight8} />
              <Text style={styles.text2}>
                You can also link your Dynalog from anywhere you want!{"\n"}
                (hold to copy the URL below){"\n"}
                https://dynalog-qa.catalogs.com/g3441/sun-toyota?merchantID=1557&amp;padUID=1266
              </Text>
            </View>
            <SocialMediaButtons style={styles.socialMediaButtons} />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  rect3: {
    width: 375,
    height: 170,
    backgroundColor: "rgba(192,192,192,1)"
  },
  stack: {
    position: "relative"
  },
  image4: {
    top: 0,
    left: 0,
    width: 170,
    height: 170,
    position: "absolute"
  },
  materialButtonLight8: {
    top: 44,
    left: 170,
    width: 171,
    height: 36,
    position: "absolute"
  },
  text2: {
    top: 133,
    left: 155,
    color: "rgba(0,0,0,1)",
    position: "absolute",
    fontSize: 7,
    fontFamily: "roboto-700"
  },
  socialMediaButtons: {
    top: 89,
    left: 210,
    width: 93,
    height: 30,
    position: "absolute"
  }
});
