import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import MaterialCheckbox from "./MaterialCheckbox";

export default class Resource3 extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View
          style={[
            styles.stack,
            {
              height: 41,
              width: 124
            }
          ]}
        >
          <MaterialCheckbox style={styles.materialCheckbox3} />
          <Text style={styles.text6}>$500 Rebate</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  stack: {
    position: "relative"
  },
  materialCheckbox3: {
    top: 0,
    left: 0,
    width: 41,
    height: 41,
    position: "absolute"
  },
  text6: {
    top: 12,
    left: 40,
    color: "rgba(74,74,74,1)",
    position: "absolute",
    fontSize: 14
  }
});
