import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import RepeatPassword from "./RepeatPassword";

export default class RepeatPasswordChange extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View
          style={[
            styles.stack,
            {
              height: 61,
              width: 311
            }
          ]}
        >
          <RepeatPassword style={styles.materialUnderlineTextbox12} />
          <Text style={styles.text}>Repeat Password*</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  stack: {
    position: "relative"
  },
  materialUnderlineTextbox12: {
    top: 18,
    left: 0,
    width: 311,
    height: 43,
    position: "absolute"
  },
  text: {
    top: 0,
    left: 0,
    color: "rgba(74,74,74,1)",
    position: "absolute",
    fontSize: 14,
    fontFamily: "roboto-regular",
    lineHeight: 18
  }
});
