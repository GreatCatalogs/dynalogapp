import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

export default class Deals2 extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View style={styles.rect3}>
          <View
            style={[
              styles.row,
              {
                marginTop: 12,
                marginLeft: 20,
                marginRight: 31,
                height: 54
              }
            ]}
          >
            <Icon name="cash" style={styles.icon2} />
            <Text style={styles.text2}>
              Send The Most Catalogs During DD26 And Win $100 Visa Card - Leader
              Board
            </Text>
          </View>
          <Text style={styles.text3}>from 04/08/2019 to 04/12/2019</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  rect3: {
    width: 375,
    height: 95,
    backgroundColor: "rgba(255,255,255,1)"
  },
  row: {
    flexDirection: "row"
  },
  icon2: {
    color: "grey",
    fontSize: 45,
    height: 45,
    width: 45,
    marginTop: 9
  },
  text2: {
    color: "rgba(74,74,74,1)",
    fontSize: 18,
    fontFamily: "roboto-regular",
    marginLeft: 6
  },
  text3: {
    color: "rgba(74,74,74,1)",
    fontSize: 8,
    fontFamily: "roboto-regular",
    marginTop: 7,
    marginLeft: 72
  }
});
