import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import MaterialCheckbox10 from "./MaterialCheckbox10";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import MaterialButtonDark from "./MaterialButtonDark";
import MaterialButtonDark1 from "./MaterialButtonDark1";

export default class Lead2EditChat extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View
          style={[
            styles.row,
            {
              height: 40,
              flex: 1
            }
          ]}
        >
          <View
            style={[
              styles.stack,
              {
                height: 40,
                width: 62
              }
            ]}
          >
            <MaterialCheckbox10 style={styles.materialCheckbox103} />
            <Icon name="account" style={styles.icon3} />
          </View>
          <Text style={styles.text2}>Lead&#39;s Name 2</Text>
          <MaterialButtonDark style={styles.materialButtonDark2} />
          <MaterialButtonDark1 style={styles.materialButtonDark12} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row"
  },
  row: {
    flexDirection: "row"
  },
  stack: {
    position: "relative"
  },
  materialCheckbox103: {
    top: 0,
    left: 0,
    width: 40,
    height: 40,
    position: "absolute"
  },
  icon3: {
    top: 5,
    left: 32,
    position: "absolute",
    color: "grey",
    fontSize: 30
  },
  text2: {
    color: "rgba(74,74,74,1)",
    fontSize: 14,
    fontFamily: "roboto-regular",
    marginLeft: 3,
    marginTop: 14
  },
  materialButtonDark2: {
    width: 88,
    height: 24,
    marginLeft: 20,
    marginTop: 9
  },
  materialButtonDark12: {
    width: 88,
    height: 25,
    marginLeft: 5,
    marginTop: 8
  }
});
