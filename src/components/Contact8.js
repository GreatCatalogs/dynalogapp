import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import MaterialCheckbox8 from "./MaterialCheckbox8";

export default class Contact8 extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View style={styles.rect8}>
          <View
            style={[
              styles.row,
              {
                marginRight: 19,
                marginLeft: 17,
                marginTop: 7,
                height: 40,
                flex: 1
              }
            ]}
          >
            <Icon name="plus-circle" style={styles.icon7} />
            <Text style={styles.text7}>Contact 8</Text>
            <MaterialCheckbox8 style={styles.materialCheckbox8} />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  rect8: {
    width: 375,
    height: 51,
    backgroundColor: "rgba(255,255,255,1)",
    flexDirection: "row"
  },
  row: {
    flexDirection: "row"
  },
  icon7: {
    color: "grey",
    fontSize: 30,
    height: 30,
    width: 30,
    marginTop: 3
  },
  text7: {
    color: "rgba(136,136,136,1)",
    fontSize: 14,
    fontFamily: "roboto-regular",
    marginLeft: 13,
    marginTop: 11
  },
  materialCheckbox8: {
    width: 40,
    height: 40,
    marginLeft: 196
  }
});
