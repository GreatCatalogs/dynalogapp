import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

export default class Profile extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View style={styles.rect3}>
          <View
            style={[
              styles.row,
              {
                marginRight: 38,
                marginLeft: 31,
                marginTop: 11,
                height: 30,
                flex: 1
              }
            ]}
          >
            <Text style={styles.text}>Profile</Text>
            <Icon name="account" style={styles.icon3} />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  rect3: {
    width: 375,
    height: 51,
    backgroundColor: "rgba(62,145,188,1)",
    flexDirection: "row"
  },
  row: {
    flexDirection: "row"
  },
  text: {
    color: "rgba(255,255,255,1)",
    fontSize: 18,
    fontFamily: "roboto-700",
    marginTop: 6
  },
  icon3: {
    color: "rgba(255,255,255,1)",
    fontSize: 30,
    height: 30,
    width: 22,
    marginLeft: 233
  }
});
