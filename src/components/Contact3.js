import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import MaterialCheckbox3 from "./MaterialCheckbox3";

export default class Contact3 extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View style={styles.rect3}>
          <View
            style={[
              styles.row,
              {
                marginRight: 21,
                marginLeft: 17,
                marginTop: 4,
                height: 40,
                flex: 1
              }
            ]}
          >
            <Icon name="plus-circle" style={styles.icon2} />
            <Text style={styles.text2}>Contact 3</Text>
            <MaterialCheckbox3 style={styles.materialCheckbox3} />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  rect3: {
    width: 375,
    height: 51,
    backgroundColor: "rgba(255,255,255,1)",
    flexDirection: "row"
  },
  row: {
    flexDirection: "row"
  },
  icon2: {
    color: "grey",
    fontSize: 30,
    height: 30,
    width: 30,
    marginTop: 6
  },
  text2: {
    color: "rgba(136,136,136,1)",
    fontSize: 14,
    fontFamily: "roboto-regular",
    marginLeft: 13,
    marginTop: 14
  },
  materialCheckbox3: {
    width: 40,
    height: 40,
    marginLeft: 190
  }
});
