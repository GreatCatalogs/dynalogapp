import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";

export default class Email extends Component {
  render() {
    return (
      <View style={[styles.root, this.props.style]}>
        <Text style={styles.label}>EMAIL</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: "rgba(255,255,255,1)",
    flexDirection: "row",
    paddingLeft: 16,
    borderRadius: 14
  },
  label: {
    color: "#000",
    alignSelf: "flex-start",
    opacity: 0.5,
    paddingTop: 13,
    paddingBottom: 13,
    transform: [
      {
        rotate: "1.00deg"
      }
    ],
    fontSize: 14,
    lineHeight: 16
  }
});
