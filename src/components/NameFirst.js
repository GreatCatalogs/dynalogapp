import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import MaterialUnderlineTextbox11 from "./MaterialUnderlineTextbox11";

export default class NameFirst extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View
          style={[
            styles.row,
            {
              height: 39,
              flex: 1
            }
          ]}
        >
          <Text style={styles.text13}>First Name *</Text>
          <MaterialUnderlineTextbox11
            style={styles.materialUnderlineTextbox11}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row"
  },
  row: {
    flexDirection: "row"
  },
  text13: {
    color: "#121212",
    fontFamily: "roboto-regular",
    marginTop: 13
  },
  materialUnderlineTextbox11: {
    width: 237,
    height: 39,
    marginLeft: 10
  }
});
