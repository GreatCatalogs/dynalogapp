import React, { Component } from "react";
import { StyleSheet, TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

export default class MaterialCheckbox1 extends Component {
  render() {
    return (
      <TouchableOpacity style={[styles.container, this.props.style]}>
        <Icon
          name={
            this.props.checked ? "checkbox-marked" : "checkbox-blank-outline"
          }
          style={styles.checkIcon}
        />
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "transparent",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 20
  },
  checkIcon: {
    color: "rgba(74,74,74,1)",
    fontFamily: "roboto-regular",
    fontSize: 28,
    lineHeight: 28
  }
});
