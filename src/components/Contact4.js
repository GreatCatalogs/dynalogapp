import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import MaterialCheckbox4 from "./MaterialCheckbox4";

export default class Contact4 extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View style={styles.rect4}>
          <View
            style={[
              styles.row,
              {
                marginRight: 21,
                marginLeft: 17,
                marginTop: 4,
                height: 40,
                flex: 1
              }
            ]}
          >
            <Icon name="plus-circle" style={styles.icon3} />
            <Text style={styles.text3}>Contact 4</Text>
            <MaterialCheckbox4 style={styles.materialCheckbox4} />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  rect4: {
    width: 375,
    height: 51,
    backgroundColor: "rgba(255,255,255,1)",
    flexDirection: "row"
  },
  row: {
    flexDirection: "row"
  },
  icon3: {
    color: "grey",
    fontSize: 30,
    height: 30,
    width: 30,
    marginTop: 6
  },
  text3: {
    color: "rgba(136,136,136,1)",
    fontSize: 14,
    fontFamily: "roboto-regular",
    marginLeft: 13,
    marginTop: 14
  },
  materialCheckbox4: {
    width: 40,
    height: 40,
    marginLeft: 194
  }
});
