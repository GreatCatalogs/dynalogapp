import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import MaterialCheckbox from "./MaterialCheckbox";

export default class Resource1 extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View
          style={[
            styles.stack,
            {
              height: 40,
              width: 286
            }
          ]}
        >
          <MaterialCheckbox style={styles.materialCheckbox} />
          <Text style={styles.text4}>4Runner, Tacoma, Tundra Commercial</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  stack: {
    position: "relative"
  },
  materialCheckbox: {
    top: 0,
    left: 0,
    width: 40,
    height: 40,
    position: "absolute"
  },
  text4: {
    top: 13,
    left: 40,
    color: "rgba(74,74,74,1)",
    position: "absolute"
  }
});
