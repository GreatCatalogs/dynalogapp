import React, { Component } from "react";
import { StyleSheet, View } from "react-native";
import MaterialCommunityIconsIcon from "react-native-vector-icons/MaterialCommunityIcons";

export default class SocialMediaButtons extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View
          style={[
            styles.row,
            {
              height: 30,
              flex: 1
            }
          ]}
        >
          <MaterialCommunityIconsIcon
            name="facebook-box"
            style={styles.icon2}
          />
          <MaterialCommunityIconsIcon name="twitter-box" style={styles.icon3} />
          <MaterialCommunityIconsIcon name="email" style={styles.icon4} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row"
  },
  row: {
    flexDirection: "row"
  },
  icon2: {
    color: "rgba(74,74,74,1)",
    fontSize: 30
  },
  icon3: {
    color: "rgba(74,74,74,1)",
    fontSize: 30,
    marginLeft: 1
  },
  icon4: {
    color: "rgba(74,74,74,1)",
    fontSize: 30,
    marginLeft: 2
  }
});
