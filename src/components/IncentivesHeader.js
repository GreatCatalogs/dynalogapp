import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

export default class IncentivesHeader extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View
          style={[
            styles.stack,
            {
              height: 42,
              width: 375
            }
          ]}
        >
          <View style={styles.rect2}>
            <Text style={styles.text}>INCENTIVES</Text>
          </View>
          <Icon name="keyboard-backspace" style={styles.icon} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  stack: {
    position: "relative"
  },
  rect2: {
    top: 0,
    left: 0,
    width: 375,
    height: 40,
    backgroundColor: "rgba(230, 230, 230,1)",
    position: "absolute"
  },
  text: {
    color: "rgba(136,136,136,1)",
    fontSize: 18,
    fontFamily: "roboto-regular",
    marginTop: 13,
    marginLeft: 66
  },
  icon: {
    top: 2,
    left: 17,
    position: "absolute",
    color: "grey",
    fontSize: 40
  }
});
