import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import Icon from "react-native-vector-icons/Ionicons";

export default class Lead1 extends Component {
  render() {
    return (
      <View style={[styles.root, this.props.style]}>
        <Text style={styles.chipText}>Lead 1</Text>
        <Icon name="ios-contact" style={styles.icon} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: "rgba(255,255,255,1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    borderRadius: 50
  },
  chipText: {
    color: "rgba(0,0,0,0.87)",
    paddingRight: 27,
    paddingLeft: 35,
    fontSize: 13
  },
  icon: {
    left: 3,
    position: "absolute",
    color: "grey",
    fontSize: 33,
    top: 0
  }
});
