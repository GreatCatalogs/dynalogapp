import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import MaterialCheckbox1 from "./MaterialCheckbox1";

export default class Contact1 extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View
          style={[
            styles.row,
            {
              height: 40,
              flex: 1
            }
          ]}
        >
          <Icon name="plus-circle" style={styles.icon} />
          <Text style={styles.text}>Contact 1</Text>
          <MaterialCheckbox1 style={styles.materialCheckbox1} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row"
  },
  row: {
    flexDirection: "row"
  },
  icon: {
    color: "grey",
    fontSize: 30,
    marginTop: 5
  },
  text: {
    color: "rgba(136,136,136,1)",
    fontSize: 14,
    fontFamily: "arial-regular",
    marginLeft: 11,
    marginTop: 13
  },
  materialCheckbox1: {
    width: 40,
    height: 40,
    marginLeft: 194
  }
});
