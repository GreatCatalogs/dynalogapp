import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

export default class Faq12 extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View style={styles.rect13}>
          <View
            style={[
              styles.row,
              {
                marginRight: 125,
                marginLeft: 17,
                marginTop: 8,
                height: 20,
                flex: 1
              }
            ]}
          >
            <Icon name="arrow-right-bold-circle" style={styles.icon13} />
            <Text style={styles.text13}>
              How do I pay for my Personal Dynalog?
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  rect13: {
    width: 375,
    height: 36,
    backgroundColor: "rgba(255,255,255,1)",
    flexDirection: "row"
  },
  row: {
    flexDirection: "row"
  },
  icon13: {
    color: "grey",
    fontSize: 20,
    height: 20,
    width: 20
  },
  text13: {
    color: "rgba(74,74,74,1)",
    fontSize: 12,
    fontFamily: "roboto-regular",
    marginLeft: 5,
    marginTop: 4
  }
});
