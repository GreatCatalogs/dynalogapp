import React, { Component } from "react";
import { StyleSheet, View } from "react-native";
import MaterialChipWithImage2 from "./MaterialChipWithImage2";
import MaterialButtonLight6 from "./MaterialButtonLight6";
import MaterialButtonLight7 from "./MaterialButtonLight7";

export default class Leadone extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View
          style={[
            styles.stack,
            {
              height: 32,
              width: 334
            }
          ]}
        >
          <MaterialChipWithImage2 style={styles.materialChipWithImage2} />
          <MaterialButtonLight6 style={styles.materialButtonLight6} />
          <MaterialButtonLight7 style={styles.materialButtonLight7} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  stack: {
    position: "relative"
  },
  materialChipWithImage2: {
    top: 0,
    left: 0,
    width: 334,
    height: 32,
    position: "absolute"
  },
  materialButtonLight6: {
    top: 7,
    left: 142,
    width: 88,
    height: 19,
    position: "absolute"
  },
  materialButtonLight7: {
    top: 7,
    left: 233,
    width: 88,
    height: 19,
    position: "absolute"
  }
});
