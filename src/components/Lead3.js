import React, { Component } from "react";
import { StyleSheet, View } from "react-native";
import MaterialChipWithImage4 from "./MaterialChipWithImage4";
import MaterialButtonLight4 from "./MaterialButtonLight4";
import MaterialButtonLight5 from "./MaterialButtonLight5";

export default class Lead3 extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View
          style={[
            styles.stack,
            {
              height: 32,
              width: 333
            }
          ]}
        >
          <MaterialChipWithImage4 style={styles.materialChipWithImage4} />
          <MaterialButtonLight4 style={styles.materialButtonLight4} />
          <MaterialButtonLight5 style={styles.materialButtonLight5} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  stack: {
    position: "relative"
  },
  materialChipWithImage4: {
    top: 0,
    left: 0,
    width: 333,
    height: 32,
    position: "absolute"
  },
  materialButtonLight4: {
    top: 7,
    left: 143,
    width: 88,
    height: 17,
    position: "absolute"
  },
  materialButtonLight5: {
    top: 8,
    left: 235,
    width: 88,
    height: 17,
    position: "absolute"
  }
});
