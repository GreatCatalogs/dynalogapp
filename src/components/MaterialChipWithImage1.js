import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import { Center } from "@builderx/utils";
import Icon from "react-native-vector-icons/Ionicons";

export default class MaterialChipWithImage1 extends Component {
  render() {
    return (
      <View style={[styles.root, this.props.style]}>
        <Text style={styles.chipText}>LEAD 1</Text>
        <Center vertical>
          <Icon name="ios-contact" style={styles.icon} />
        </Center>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: "rgba(255,255,255,1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    borderRadius: 50
  },
  chipText: {
    color: "rgba(0,0,0,0.87)",
    paddingRight: 12,
    paddingLeft: 41,
    fontSize: 13
  },
  icon: {
    left: 0,
    position: "absolute",
    color: "grey",
    fontSize: 40
  }
});
