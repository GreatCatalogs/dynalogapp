import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import MaterialCheckbox from "./MaterialCheckbox";

export default class Resource4 extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View
          style={[
            styles.stack,
            {
              height: 41,
              width: 197
            }
          ]}
        >
          <MaterialCheckbox style={styles.materialCheckbox4} />
          <Text style={styles.text7}>Service Center Coupons</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  stack: {
    position: "relative"
  },
  materialCheckbox4: {
    top: 0,
    left: 0,
    width: 41,
    height: 41,
    position: "absolute"
  },
  text7: {
    top: 14,
    left: 40,
    color: "rgba(74,74,74,1)",
    position: "absolute",
    fontSize: 14
  }
});
