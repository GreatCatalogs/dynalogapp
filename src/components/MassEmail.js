import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

export default class MassEmail extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View style={styles.rect9}>
          <View
            style={[
              styles.row,
              {
                marginRight: 37,
                marginLeft: 32,
                marginTop: 11,
                height: 30,
                flex: 1
              }
            ]}
          >
            <Text style={styles.text8}>Mass Email</Text>
            <Icon name="send" style={styles.icon10} />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  rect9: {
    width: 375,
    height: 51,
    backgroundColor: "rgba(62,145,188,1)",
    flexDirection: "row"
  },
  row: {
    flexDirection: "row"
  },
  text8: {
    color: "rgba(255,255,255,1)",
    fontSize: 18,
    fontFamily: "roboto-700",
    marginTop: 6
  },
  icon10: {
    color: "rgba(255,255,255,1)",
    fontSize: 30,
    height: 30,
    width: 22,
    marginLeft: 194
  }
});
