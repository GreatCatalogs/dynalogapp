import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import MaterialUnderlineTextbox5 from "./MaterialUnderlineTextbox5";

export default class Cellphone extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View
          style={[
            styles.row,
            {
              height: 39,
              flex: 1
            }
          ]}
        >
          <Text style={styles.text6}>Cell Phone *</Text>
          <MaterialUnderlineTextbox5 style={styles.materialUnderlineTextbox5} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row"
  },
  row: {
    flexDirection: "row"
  },
  text6: {
    color: "#121212",
    marginTop: 13
  },
  materialUnderlineTextbox5: {
    width: 237,
    height: 39,
    marginLeft: 15
  }
});
