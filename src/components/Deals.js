import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

export default class Deals extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View style={styles.rect3}>
          <View
            style={[
              styles.row,
              {
                marginTop: 12,
                marginLeft: 18,
                marginRight: 52,
                height: 45
              }
            ]}
          >
            <Icon name="cash" style={styles.icon} />
            <View
              style={[
                styles.column,
                {
                  marginLeft: 8,
                  marginTop: 7,
                  marginBottom: 10,
                  width: 252
                }
              ]}
            >
              <Text style={styles.text4}>Set 10 apts in 3 days and win</Text>
              <Text style={styles.text5}>from 06-19-2019 to 06-21-2019</Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  rect3: {
    width: 375,
    height: 65,
    backgroundColor: "rgba(255,255,255,1)",
    marginTop: 19,
    marginLeft: 11
  },
  row: {
    flexDirection: "row"
  },
  icon: {
    color: "grey",
    fontSize: 45,
    height: 45,
    width: 45
  },
  column: {
    flexDirection: "column"
  },
  text4: {
    color: "rgba(74,74,74,1)",
    fontSize: 18
  },
  text5: {
    color: "rgba(74,74,74,1)",
    fontSize: 9,
    marginTop: 1
  }
});
