import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import Icon from "react-native-vector-icons/Entypo";
import MaterialButtonPurple3 from "./MaterialButtonPurple3";

export default class Sendaresource extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View
          style={[
            styles.row,
            {
              height: 28,
              flex: 1
            }
          ]}
        >
          <Icon name="folder-images" style={styles.icon} />
          <Text style={styles.text2}>SEND A RESOURCE</Text>
          <MaterialButtonPurple3 style={styles.materialButtonPurple3} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row"
  },
  row: {
    flexDirection: "row"
  },
  icon: {
    color: "rgba(136,136,136,1)",
    fontSize: 27,
    marginTop: 1
  },
  text2: {
    color: "rgba(136,136,136,1)",
    fontSize: 18,
    marginLeft: 9,
    marginTop: 5
  },
  materialButtonPurple3: {
    width: 89,
    height: 28,
    marginLeft: 48
  }
});
