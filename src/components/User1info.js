import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

export default class User1info extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View style={styles.rect}>
          <View
            style={[
              styles.row,
              {
                marginRight: 19,
                marginLeft: 19,
                marginTop: 4,
                height: 30,
                flex: 1
              }
            ]}
          >
            <Text style={styles.text}>User 1 Name</Text>
            <Icon name="arrow-right-bold-circle" style={styles.icon} />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  rect: {
    width: 375,
    height: 40,
    backgroundColor: "rgba(255,255,255,1)",
    flexDirection: "row"
  },
  row: {
    flexDirection: "row"
  },
  text: {
    color: "rgba(136,136,136,1)",
    fontSize: 12,
    fontFamily: "roboto-regular",
    marginTop: 9
  },
  icon: {
    color: "grey",
    fontSize: 30,
    height: 30,
    width: 30,
    marginLeft: 238
  }
});
