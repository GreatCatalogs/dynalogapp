import React, { Component } from "react";
import { StyleSheet, View, TextInput } from "react-native";

export default class ContactUsMessage extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <TextInput
          placeholder="Type message..."
          maxLength={340}
          multiline={true}
          numberOfLines={7}
          style={styles.inputStyle}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "transparent",
    flexDirection: "row",
    alignItems: "center",
    borderColor: "#D9D5DC",
    borderBottomWidth: 1
  },
  inputStyle: {
    height: 260,
    flex: 1,
    backgroundColor: "rgba(255,255,255,1)",
    color: "#000",
    paddingTop: 16,
    paddingRight: 5,
    paddingBottom: 8,
    paddingLeft: 9,
    fontSize: 16,
    lineHeight: 16
  }
});
