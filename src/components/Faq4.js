import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

export default class Faq4 extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View style={styles.rect5}>
          <View
            style={[
              styles.row,
              {
                marginRight: 33,
                marginLeft: 17,
                marginTop: 5,
                height: 24,
                flex: 1
              }
            ]}
          >
            <Icon name="arrow-right-bold-circle" style={styles.icon5} />
            <Text style={styles.text5}>
              How do I share my Personal Dynalog with prospects or clients?
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  rect5: {
    width: 375,
    height: 36,
    backgroundColor: "rgba(255,255,255,1)",
    flexDirection: "row"
  },
  row: {
    flexDirection: "row"
  },
  icon5: {
    color: "grey",
    fontSize: 20,
    height: 20,
    width: 20,
    marginTop: 4
  },
  text5: {
    color: "rgba(74,74,74,1)",
    fontSize: 12,
    fontFamily: "roboto-regular",
    marginLeft: 5
  }
});
