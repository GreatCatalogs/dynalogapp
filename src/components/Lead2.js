import React, { Component } from "react";
import { StyleSheet, View } from "react-native";
import MaterialChipWithImage3 from "./MaterialChipWithImage3";
import MaterialButtonLight1 from "./MaterialButtonLight1";
import MaterialButtonLight3 from "./MaterialButtonLight3";

export default class Lead2 extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View
          style={[
            styles.stack,
            {
              height: 32,
              width: 334
            }
          ]}
        >
          <MaterialChipWithImage3 style={styles.materialChipWithImage3} />
          <MaterialButtonLight1 style={styles.materialButtonLight1} />
          <MaterialButtonLight3 style={styles.materialButtonLight3} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  stack: {
    position: "relative"
  },
  materialChipWithImage3: {
    top: 0,
    left: 0,
    width: 334,
    height: 32,
    position: "absolute"
  },
  materialButtonLight1: {
    top: 6,
    left: 144,
    width: 88,
    height: 19,
    position: "absolute"
  },
  materialButtonLight3: {
    top: 6,
    left: 236,
    width: 88,
    height: 19,
    position: "absolute"
  }
});
