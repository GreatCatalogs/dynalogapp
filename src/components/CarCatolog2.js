import React, { Component } from "react";
import { StyleSheet, View, Image, Text } from "react-native";
import TaptoViewButton from "./TaptoViewButton";
import SocialMediaButtons from "./SocialMediaButtons";

export default class CarCatolog2 extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View style={styles.rect4}>
          <View
            style={[
              styles.stack,
              {
                marginTop: 1,
                height: 169,
                width: 364
              }
            ]}
          >
            <View
              style={[
                styles.stack,
                {
                  top: 0,
                  left: 0,
                  height: 169,
                  width: 364,
                  position: "absolute"
                }
              ]}
            >
              <Image
                source={require("../assets/images/catalog-sample21.png")}
                resizeMode="contain"
                style={styles.image5}
              />
              <Text style={styles.text3}>
                You can also link your Dynalog from anywhere you want!{"\n"}
                (hold to copy the URL below){"\n"}
                https://dynalog-qa.catalogs.com/g3442/sun-toyota-specials?merchantID=1557&amp;padUID=1266
              </Text>
            </View>
            <TaptoViewButton style={styles.materialButtonLight82} />
            <SocialMediaButtons style={styles.socialMediaButtons2} />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  rect4: {
    width: 375,
    height: 170,
    backgroundColor: "rgba(192,192,192,1)"
  },
  stack: {
    position: "relative"
  },
  image5: {
    top: 0,
    left: 0,
    width: 169,
    height: 169,
    position: "absolute"
  },
  text3: {
    top: 134,
    left: 154,
    color: "rgba(0,0,0,1)",
    position: "absolute",
    fontSize: 7,
    fontFamily: "impact-regular"
  },
  materialButtonLight82: {
    top: 47,
    left: 170,
    width: 171,
    height: 36,
    position: "absolute"
  },
  socialMediaButtons2: {
    top: 92,
    left: 209,
    width: 93,
    height: 30,
    position: "absolute"
  }
});
