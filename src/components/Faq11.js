import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

export default class Faq11 extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View style={styles.rect12}>
          <View
            style={[
              styles.row,
              {
                marginRight: 38,
                marginLeft: 17,
                marginTop: 7,
                height: 24,
                flex: 1
              }
            ]}
          >
            <Icon name="arrow-right-bold-circle" style={styles.icon12} />
            <Text style={styles.text12}>
              Can my Personal Dynalog show listings from other RE brokerages?
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  rect12: {
    width: 375,
    height: 36,
    backgroundColor: "rgba(255,255,255,1)",
    flexDirection: "row"
  },
  row: {
    flexDirection: "row"
  },
  icon12: {
    color: "grey",
    fontSize: 20,
    height: 20,
    width: 20,
    marginTop: 2
  },
  text12: {
    color: "rgba(74,74,74,1)",
    fontSize: 12,
    fontFamily: "roboto-regular",
    marginLeft: 5
  }
});
