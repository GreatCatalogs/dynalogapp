import React, { Component } from "react";
import { StyleSheet, View, TextInput } from "react-native";

export default class MaterialUnderlineTextbox2 extends Component {
  render() {
    return (
      <View style={[styles.root, this.props.style]}>
        <TextInput placeholder="" editable={false} style={styles.inputStyle} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: "transparent",
    flexDirection: "row",
    alignItems: "center"
  },
  inputStyle: {
    height: 50,
    flex: 1,
    backgroundColor: "rgba(255,255,255,1)",
    color: "#000",
    paddingTop: 25,
    paddingRight: 25,
    paddingBottom: 25,
    paddingLeft: 11,
    borderRadius: 10,
    fontSize: 14,
    lineHeight: 16
  }
});
