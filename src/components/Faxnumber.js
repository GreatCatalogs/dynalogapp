import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import MaterialUnderlineTextbox8 from "./MaterialUnderlineTextbox8";

export default class Faxnumber extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View
          style={[
            styles.row,
            {
              height: 39,
              flex: 1
            }
          ]}
        >
          <Text style={styles.text13}>Fax Number</Text>
          <MaterialUnderlineTextbox8 style={styles.materialUnderlineTextbox8} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row"
  },
  row: {
    flexDirection: "row"
  },
  text13: {
    color: "#121212",
    marginTop: 13
  },
  materialUnderlineTextbox8: {
    width: 237,
    height: 39,
    marginLeft: 15
  }
});
