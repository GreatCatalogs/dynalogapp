import React, { Component } from "react";
import { StyleSheet, View, Image } from "react-native";
import MaterialButtonPurple2 from "./MaterialButtonPurple2";

export default class Catalog2 extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View
          style={[
            styles.stack,
            {
              height: 200,
              width: 168
            }
          ]}
        >
          <Image
            source={require("../assets/images/catalog-sample2.png")}
            resizeMode="contain"
            style={styles.image5}
          />
          <MaterialButtonPurple2 style={styles.materialButtonPurple22} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  stack: {
    position: "relative"
  },
  image5: {
    top: 0,
    left: 0,
    width: 168,
    height: 168,
    position: "absolute"
  },
  materialButtonPurple22: {
    top: 168,
    left: 29,
    width: 112,
    height: 32,
    position: "absolute"
  }
});
