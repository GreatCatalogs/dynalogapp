import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

export default class ChangePassword extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View style={styles.rect10}>
          <View
            style={[
              styles.row,
              {
                marginRight: 37,
                marginLeft: 32,
                marginTop: 11,
                height: 30,
                flex: 1
              }
            ]}
          >
            <Text style={styles.text9}>Change Password</Text>
            <Icon name="key" style={styles.icon11} />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  rect10: {
    width: 375,
    height: 51,
    backgroundColor: "rgba(62,145,188,1)",
    flexDirection: "row"
  },
  row: {
    flexDirection: "row"
  },
  text9: {
    color: "rgba(255,255,255,1)",
    fontSize: 18,
    fontFamily: "roboto-regular",
    marginTop: 6
  },
  icon11: {
    color: "rgba(255,255,255,1)",
    fontSize: 30,
    height: 30,
    width: 22,
    marginLeft: 139
  }
});
