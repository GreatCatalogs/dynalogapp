import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import MaterialUnderlineTextbox9 from "./MaterialUnderlineTextbox9";

export default class Teamname extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View
          style={[
            styles.row,
            {
              height: 39,
              flex: 1
            }
          ]}
        >
          <Text style={styles.text10}>Team Name</Text>
          <MaterialUnderlineTextbox9 style={styles.materialUnderlineTextbox9} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row"
  },
  row: {
    flexDirection: "row"
  },
  text10: {
    color: "#121212",
    marginTop: 13
  },
  materialUnderlineTextbox9: {
    width: 237,
    height: 39,
    marginLeft: 15
  }
});
