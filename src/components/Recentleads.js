import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import Icon from "react-native-vector-icons/Ionicons";

export default class Recentleads extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View
          style={[
            styles.row,
            {
              height: 34,
              flex: 1
            }
          ]}
        >
          <Icon name="ios-contact" style={styles.icon2} />
          <Text style={styles.text3}>RECENT LEADS</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row"
  },
  row: {
    flexDirection: "row"
  },
  icon2: {
    color: "rgba(136,136,136,1)",
    fontSize: 34
  },
  text3: {
    color: "rgba(136,136,136,1)",
    fontSize: 18,
    marginLeft: 9,
    marginTop: 8
  }
});
