import React, { Component } from "react";
import { StyleSheet, TouchableOpacity, Text } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

export default class MaterialCheckboxWithLabel extends Component {
  render() {
    return (
      <TouchableOpacity style={[styles.container, this.props.style]}>
        <Icon
          name={
            this.props.checked ? "checkbox-marked" : "checkbox-blank-outline"
          }
          style={styles.checkIcon}
        />
        <Text style={styles.checkLabel}>
          {this.props.label || "4Runner, Tacoma, Tundra"}
        </Text>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "transparent",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 20
  },
  checkIcon: {
    color: "#3F51B5",
    fontFamily: "roboto-regular",
    fontSize: 28,
    lineHeight: 28
  },
  checkLabel: {
    color: "rgba(0,0,0,0.87)",
    marginLeft: 2,
    fontSize: 16
  }
});
