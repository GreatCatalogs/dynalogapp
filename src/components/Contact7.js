import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import MaterialCheckbox7 from "./MaterialCheckbox7";

export default class Contact7 extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View style={styles.rect7}>
          <View
            style={[
              styles.row,
              {
                marginRight: 20,
                marginLeft: 17,
                marginTop: 5,
                height: 40,
                flex: 1
              }
            ]}
          >
            <Icon name="plus-circle" style={styles.icon6} />
            <Text style={styles.text6}>Contact 7</Text>
            <MaterialCheckbox7 style={styles.materialCheckbox7} />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  rect7: {
    width: 375,
    height: 51,
    backgroundColor: "rgba(255,255,255,1)",
    flexDirection: "row"
  },
  row: {
    flexDirection: "row"
  },
  icon6: {
    color: "grey",
    fontSize: 30,
    height: 30,
    width: 30,
    marginTop: 5
  },
  text6: {
    color: "rgba(136,136,136,1)",
    fontSize: 14,
    fontFamily: "roboto-regular",
    marginLeft: 13,
    marginTop: 13
  },
  materialCheckbox7: {
    width: 40,
    height: 40,
    marginLeft: 195
  }
});
