import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

export default class Faq6 extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <View style={styles.rect7}>
          <View
            style={[
              styles.row,
              {
                marginLeft: 17,
                marginTop: 7,
                height: 24,
                flex: 1
              }
            ]}
          >
            <Icon name="arrow-right-bold-circle" style={styles.icon7} />
            <Text style={styles.text7}>
              How do I share my Personal Dynalog with a single individual or to
              groups?
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  rect7: {
    width: 375,
    height: 36,
    backgroundColor: "rgba(255,255,255,1)",
    flexDirection: "row"
  },
  row: {
    flexDirection: "row"
  },
  icon7: {
    color: "grey",
    fontSize: 20,
    height: 20,
    width: 20,
    marginTop: 2
  },
  text7: {
    color: "rgba(74,74,74,1)",
    fontSize: 12,
    fontFamily: "roboto-regular",
    marginLeft: 5
  }
});
