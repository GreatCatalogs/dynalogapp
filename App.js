import React from "react";
import { StackNavigator, DrawerNavigator } from "react-navigation";
import { AppLoading } from "expo";
import * as Font from "expo-font";
import ChangePassword from "./src/screens/ChangePassword";
import ContactUs from "./src/screens/ContactUs";
import Faq from "./src/screens/Faq";
import Home from "./src/screens/Home";
import Incentives from "./src/screens/Incentives";
import Leads from "./src/screens/Leads";
import Leads2 from "./src/screens/Leads2";
import Login from "./src/screens/Login";
import Main from "./src/screens/Main";
import Menu from "./src/screens/Menu";
import MyProfile from "./src/screens/MyProfile";
import Resources from "./src/screens/Resources";
import SendCatalog1 from "./src/screens/SendCatalog1";
import SendCatalog2 from "./src/screens/SendCatalog2";
import SendCatalog3 from "./src/screens/SendCatalog3";
import SendCatalog4 from "./src/screens/SendCatalog4";
import Users from "./src/screens/Users";
import ViewCatalogs from "./src/screens/ViewCatalogs";
import Amplify from 'aws-amplify';
import amplify from './src/aws-exports';
import { withAuthenticator } from 'aws-amplify-react-native';

Amplify.configure(amplify);
const DrawerNavigation = DrawerNavigator({
  ChangePassword: {
    screen: ChangePassword
  },
  ContactUs: {
    screen: ContactUs
  },
  Faq: {
    screen: Faq
  },
  Home: {
    screen: Home
  },
  Incentives: {
    screen: Incentives
  },
  Leads: {
    screen: Leads
  },
  Leads2: {
    screen: Leads2
  },
  Login: {
    screen: Login
  },
  Main: {
    screen: Main
  },
  Menu: {
    screen: Menu
  },
  MyProfile: {
    screen: MyProfile
  },
  Resources: {
    screen: Resources
  },
  SendCatalog1: {
    screen: SendCatalog1
  },
  SendCatalog2: {
    screen: SendCatalog2
  },
  SendCatalog3: {
    screen: SendCatalog3
  },
  SendCatalog4: {
    screen: SendCatalog4
  },
  Users: {
    screen: Users
  },
  ViewCatalogs: {
    screen: ViewCatalogs
  }
});

const StackNavigation = StackNavigator(
  {
    DrawerNavigation: {
      screen: DrawerNavigation
    },
    ChangePassword: {
      screen: ChangePassword
    },
    ContactUs: {
      screen: ContactUs
    },
    Faq: {
      screen: Faq
    },
    Home: {
      screen: Home
    },
    Incentives: {
      screen: Incentives
    },
    Leads: {
      screen: Leads
    },
    Leads2: {
      screen: Leads2
    },
    Login: {
      screen: Login
    },
    Main: {
      screen: Main
    },
    Menu: {
      screen: Menu
    },
    MyProfile: {
      screen: MyProfile
    },
    Resources: {
      screen: Resources
    },
    SendCatalog1: {
      screen: SendCatalog1
    },
    SendCatalog2: {
      screen: SendCatalog2
    },
    SendCatalog3: {
      screen: SendCatalog3
    },
    SendCatalog4: {
      screen: SendCatalog4
    },
    Users: {
      screen: Users
    },
    ViewCatalogs: {
      screen: ViewCatalogs
    }
  },
  {
    headerMode: "null"
  }
);

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      fontLoaded: false
    };
  }

  async componentDidMount() {
    await Font.loadAsync({
      "roboto-regular": require("./src/assets/fonts/roboto-regular.ttf"),
      "arial-regular": require("./src/assets/fonts/arial-regular.ttf"),
      "impact-regular": require("./src/assets/fonts/impact-regular.ttf"),
      "roboto-700": require("./src/assets/fonts/roboto-700.ttf"),
      "calibri-regular": require("./src/assets/fonts/calibri-regular.ttf")
    });
    this.setState({ fontLoaded: true });
  }

  render() {
    return this.state.fontLoaded ? <StackNavigation /> : <AppLoading />;
  }
}

export default withAuthenticator(App);
